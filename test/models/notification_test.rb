require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  # セットアップ
  #=======================
  def setup
    @notification = Notification.new(
      sender_id:   users(:default).id,
      receiver_id: users(:hanako).id,
      m_name:      microposts(:hanako_first).class.to_s,
      r_id:        microposts(:hanako_first).id
    )
  end

  # テスト
  #=======================

  test 'ユーザの有効性の確認' do
    assert @notification.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------

  test '送信者のIDはnilでないこと' do
    @notification.sender_id = nil
    assert_not @notification.valid?
  end

  test '受信者のIDはnilでないこと' do
    @notification.receiver_id = nil
    assert_not @notification.valid?
  end

  test 'モデル名はnilでないこと' do
    @notification.m_name = nil
    assert_not @notification.valid?
  end

  test 'レコードIDはnilでないこと' do
    @notification.r_id = nil
    assert_not @notification.valid?
  end

  test '通知は新しいもの順に取り出されること' do
    assert_equal notifications(:most_recent), Notification.first
  end

  # 各メソッド検証
  #-----------------------

  # update_confirmed_to_true
  test '指定された通知受信者の通知確認済フラグをtrueにする' do
    receiver = users(:hanako)
    receiver.receive_notifications.each do |notification|
      assert_not notification.confirmed
    end
    Notification.update_confirmed_to_true(receiver)
    receiver.receive_notifications.reload.each do |notification|
      assert notification.confirmed
    end
  end

  # get_micropost
  test 'お気に入り通知インスタンスから投稿を取得する' do
    notification = notifications(:default_likes_hanako_third)
    assert_equal microposts(:hanako_third), notification.get_micropost
  end

  test 'コメント通知インスタンスから投稿を取得する' do
    notification = notifications(:hanako_comments_first)
    assert_equal microposts(:first), notification.get_micropost
  end
end
