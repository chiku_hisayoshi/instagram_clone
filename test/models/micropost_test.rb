require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  # carrierwaveテスト用
  include ActionDispatch::TestProcess

  # セットアップ
  #=======================
  def setup
    @user = users(:default)
    picture = fixture_file_upload('../fixtures/sample1.jpg', 'image/jpg')
    @micropost = @user.microposts.build(content: 'Lorem ipsum',
                                        picture: picture)
  end

  # テスト
  #=======================

  test 'ユーザおよび、マイクロポストの有効性の確認' do
    assert @user.valid?
    assert @micropost.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------
  test 'コンテンツは空欄を許容しない' do
    @micropost.content = ' '
    assert_not @micropost.valid?
  end

  test 'コンテンツはnilを許容しない' do
    @micropost.content = nil
    assert_not @micropost.valid?
  end

  test 'コンテンツは150文字以内とする' do
    @micropost.content = 'a' * 151
    assert_not @micropost.valid?
  end

  test 'ユーザID(外部キー)がnilになることは許容しない' do
    @micropost.user_id = nil
    assert_not @micropost.valid?
  end

  test 'デフォルトの取得順は投稿日の降順(新しい->古い)とする' do
    assert_equal microposts(:most_recent), Micropost.first
  end

  test '画像はnilを許容しない' do
    @micropost.picture = nil
    assert_not @micropost.valid?
  end

  test '画像サイズ5MB以上は許容しない' do
    picture = fixture_file_upload('../fixtures/bigsize.jpg', 'image/jpg')
    @micropost = @user.microposts.build(content: 'Lorem ipsum',
                                        picture: picture)
    assert_not @micropost.valid?
  end

  # 関係性検証
  #-----------------------
  # comment
  test '投稿が削除された場合、紐づいているコメントも削除されること' do
    micropost = microposts(:first)
    comment   = micropost.comments
    assert_difference 'comment.count', -3 do
      micropost.destroy
    end
  end

  # comment
  test '投稿が削除された場合、紐づいているお気に入りも削除されること' do
    micropost = microposts(:hanako_first)
    assert_difference 'Like.count', -1 do
      micropost.destroy
    end
  end
end
