require 'test_helper'

class LikeTest < ActiveSupport::TestCase
  # セットアップ
  #=======================
  def setup
    @user      = users(:default)
    @micropost = microposts(:hanako_fourth)
    params = { micropost: @micropost }
    @like  = @user.likes.build(params)
  end

  # テスト
  #=======================

  test 'likeの有効性の確認' do
    assert @like.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------

  test 'ユーザIDはnilでないこと' do
    @like.user_id = nil
    assert_not @like.valid?
  end

  test '投稿IDはnilでないこと' do
    @like.micropost_id = nil
    assert_not @like.valid?
  end

  test 'お気に入りの取得順は日付の降順(新->古)とする' do
    assert_equal likes(:most_recent), Like.first
  end

  test 'ユーザIDと投稿IDの組み合わせの重複は許容しない' do
    dup_like = @like.dup
    @like.save
    assert_not dup_like.valid?
  end
end
