require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  # セットアップ
  #=======================
  def setup
    @relationship = Relationship.new(follower_id: users(:default).id,
                                     followed_id: users(:hanako).id)
  end

  # テスト
  #=======================

  test '関係性の有効性の確認' do
    assert @relationship.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------
  test '(フォロー/フォロワー関係がある場合)フォローのIDがnilになることはない' do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end

  test '(フォロー/フォロワー関係がある場合)フォロワーのIDがnilになることはない' do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end
end
