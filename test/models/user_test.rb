require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # carrierwaveテスト用
  include ActionDispatch::TestProcess

  # セットアップ
  #=======================
  def setup
    # ユーザ作成に必要な最低限の情報
    @user = User.new(full_name: 'test',
                     user_name: 'fooooootest',
                     password: 'password',
                     password_confirmation: 'password')
  end

  # テスト
  #=======================

  test 'ユーザの有効性の確認' do
    assert @user.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------

  test 'フルネームは空白でないこと' do
    # 空白を許容しない
    @user.full_name = ' '
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'フルネームは50文字以下であること' do
    @user.full_name = 'a' * 51
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'ユーザネームは空白でないこと' do
    @user.user_name = ' '
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'ユーザネームは50文字以下であること' do
    @user.user_name = 'a' * 51
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'ユーザネームはnilでないこと' do
    @user.user_name = nil
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'ユーザネームは一意性を確保していること' do
    dup_user = @user.dup
    dup_user.email = 'hoge@hoge.com'
    dup_user.user_name = @user.user_name.upcase
    @user.save
    assert_not dup_user.valid?, dup_user.errors.full_messages
  end

  test 'ユーザネームはフォーマットに従っていること(有効)' do
    valid_user_names = %w[
      foobar FOOBAR fooBar foobar123 123foobar
      fo1ba2r _foobar foobar_ foo_bar fo____bar
      _________ F_OoB_Ar
    ]
    valid_user_names.each do |user_name|
      @user.user_name = user_name
      assert @user.valid?, user_name.inspect
    end
  end

  test 'ユーザネームはフォーマットに従っていること(無効)' do
    invalid_user_names = %w[
      foobar! ふうばああああ あいうえお fooばあああ
      _あああ_ fo!0bar\ foobar@ foobar? foobar&
      foobar% 風婆foobar Ｆoobar ＦooＢaＲ
      * + $ % & " # ' ) ( foo🅱️ar
    ]
    invalid_user_names.push('a foo bar', 'foobar ', ' foobar')
    invalid_user_names.each do |user_name|
      @user.user_name = user_name
      assert_not @user.valid?, user_name.inspect
    end
  end

  test 'ユーザネームは小文字でDBに保存されること' do
    mixed_user_name = 'FooBarFoo'
    @user.user_name = mixed_user_name
    @user.save
    assert_equal mixed_user_name.downcase,
                 @user.reload.user_name, @user.errors.full_messages
  end

  test 'メールアドレスは空白を許容すること' do
    @user.email = ' '
    assert @user.valid?, @user.errors.full_messages
  end

  test 'メールアドレスは255文字以下であること' do
    @user.email = 'a' * 256
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'メールアドレスは一意性を確保していること' do
    @user.email = 'hogehoge@hoge.com'
    dup_user = @user.dup
    dup_user.user_name = 'hogehogehoge'
    dup_user.email = @user.email.upcase
    @user.save
    assert_not dup_user.valid?, dup_user.errors.full_messages
  end

  test 'メールアドレスはフォーマットに従っていること(有効)' do
    valid_addresses = %w[
      user@example.com USER@foo.COM A_US-ER@foo.bar.org
      first.last@foo.jp alice+bob@baz.cn
    ]
    valid_addresses.each do |address|
      @user.email = address
      assert @user.valid?, @user.errors.full_messages
    end
  end

  test 'メールアドレスはフォーマットに従っていること(無効)' do
    invalid_addresses = %w[
      user@example,com user_at_foo.org user.name@example.
      foo@bar_baz.com foo@bar+baz.com
    ]
    # 追加(文字間に空欄を含むもの)
    invalid_addresses << 'a foobar@bar.com'
    invalid_addresses.each do |address|
      @user.email = address
      assert_not @user.valid?, @user.errors.full_messages
    end
  end

  test 'メールアドレスは小文字でDBに保存されること' do
    mixed_email = 'FooBaR@Bax.Com'
    @user.email = mixed_email
    @user.save
    assert_equal mixed_email.downcase,
                 @user.reload.email, @user.errors.full_messages
  end

  test '無効な文字を含むメールアドレスは許容しない' do
    invalid_addresses = %w[
      ふぉおばr@foo.com
      foobar@ばあ.com
      ｆoobar@bar.com
      foobar@ｂar.com
    ]
    invalid_addresses.each do |address|
      @user.email = address
      assert_not @user.valid?
    end
  end

  test '性別の空白を許容すること' do
    @user.gender = ' '
    assert @user.valid?, @user.errors.full_messages
  end

  test '性別は1桁であること' do
    @user.gender = 11
    assert_not @user.valid?, @user.errors.full_messages
  end

  test '性別は数値型で保存されること' do
    @user.gender = '1'
    assert @user.valid?
    @user.save
    @user.reload
    assert_equal 1, @user.gender
    assert_equal Integer, @user.gender.class
  end

  test '電話番号は空白を許容すること' do
    @user.tel = ' '
    assert @user.valid?, @user.errors.full_messages
  end

  test '電話番号は17文字以内であること' do
    @user.tel = 'a' * 18
    assert_not @user.valid?, @user.errors.full_messages
  end

  test '電話番号はフォーマットに従っていること(有効)' do
    valid_tel = %w[
      12345678 123456789 1234567890
      12345678901 123456789012
      1234567890123 12345678901234
      123456789012345 1234567890123456
      12345678901234567
    ]
    valid_tel.each do |tel|
      @user.tel = tel
      assert @user.valid?, @user.errors.full_messages
    end
  end

  test '電話番号はフォーマットに従っていること(無効)' do
    invalid_tel = %w[
      1 12 123 1234 12345 123456 123456789012345678
      a 1a2 12345678901a a12345678901 1a33safffsaase
      1234567890123\n １２３４５６７８９０１ 1２234567890
    ]
    invalid_tel.push('a 1234567890', '1 12345678 90')
    invalid_tel.each do |tel|
      @user.tel = tel
      assert_not @user.valid?, @user.errors.full_messages
    end
  end

  test '自己紹介は空白を許容する' do
    @user.introduction = ' '
    assert @user.valid?, @user.errors.full_messages
  end

  test '自己紹介は300文字以下であること' do
    @user.introduction = 'a' * 301
    assert_not @user.valid?, @user.errors.full_messages
  end

  test 'パスワードは空白でないこと' do
    @user.password = @user.password_confirmation = ' '
    assert_not @user.valid?
  end

  test 'パスワードは6文字以上であること' do
    @user.password = @user.password_confirmation = 'a' * 5
    assert_not @user.valid?
  end

  test 'パスワードは半角英文字+記号のみ許容すること(有効)' do
    valid_password = %w[
      FOOBAR foobar FooBar f00bar 1234567 foobar! foobar#
      foo$bar f%%bar foo&bar foo+bar foo-bar
      foo=bar?? @foobar_ foo_bar| f**bar
    ]
    valid_password.each do |password|
      @user.password = @user.password_confirmation = password
      assert @user.valid?, password.inspect
    end
  end

  test 'パスワードは半角英文字+記号のみ許容すること(無効)' do
    invalid_password = %w[
      f fo foo foob fooba 1 12 123 1234 12345 f0 f00b@
      ふうばああああ foobarばあ 123+345~ fooooobarrrrrrrr~
      fooおbarあ ~foobar2000 12345+5678; ＦＦＦＦＦＦＦ
      おＦfoobar fooＢar1234 foo🅱️ar |123456|~+a foobar\n
    ]
    invalid_password << 'a foobar'
    invalid_password.each do |password|
      @user.password = @user.password_confirmation = password
      assert_not @user.valid?, password.inspect
    end
  end

  test 'ウェブサイトはnilを許容すること' do
    @user.website = nil
    assert @user.valid?
  end

  test 'ウェブサイトは空文字を許容すること' do
    @user.website = ' '
    assert @user.valid?
  end

  test 'ウェブサイトは2083文字以内であること' do
    @user.website = 'a' * 2084
    assert_not @user.valid?
  end

  test 'ウェブサイトはURLのみ許容する(有効)' do
    valid_urls = %w[
      http://www.foobar.co.jp https://www.foobar.co.jp
      http://www.foobar.com https://www.foobar.com
      http://foobar.co.jp https://foobar.co.jp
      http://foobar.com http://foobar.com
      http://foobar.cn https://foobar.cn
      http://foobar.xyz https://foobar.xyz
      http://12344.com https://12344.com
      http://foobar.com?x=123&y=123s%
    ]
    valid_urls.each do |url|
      @user.website = url
      assert @user.valid?, url.inspect
    end
  end

  test 'ウェブサイトはURLのみ許容する(無効)' do
    invalid_urls = %w[
      www.foobar.co.jp foobar.co.jp
      http//foobar.co.jp https//foobar.co.jp
      http:/foobar.com http:/foobar.com
      http:foobar.cn https:foobar.cn
      htp://foobar.xyz ttps://foobar.xyz
      http://ふぉおばr.com https://ふぉおばr.com
    ]
    invalid_urls.each do |url|
      @user.website = url
      assert_not @user.valid?, url.inspect
    end
  end

  # メソッド検証
  #-----------------------

  # new_token & digest
  test 'new_tokenを引数としてdigestが生成されること' do
    token = User.new_token
    assert_not token.nil?
    digest = User.digest(token)
    assert_not digest.nil?
  end

  # set, del-remember_digest
  test 'remember_digestの保存と削除が適切に行われるか' do
    # set
    @user.set_remember_digest
    assert_not @user.remember_token.nil?
    @user.reload
    assert_not @user.remember_digest.nil?
    # del
    @user.del_remember_digest
    assert @user.remember_token.nil?
    @user.reload
    assert @user.remember_digest.nil?
  end

  # authenticated?
  test 'トークンを適切に認証するか' do
    @user.set_remember_digest
    @user.reload
    # 間違ったトークンを与える
    assert_not @user.authenticated?(:remember, 'foobar')
    # 空文字を与える
    assert_not @user.authenticated?(:remember, '')
    # nilを与える
    assert_not @user.authenticated?(:remember, nil)
    # 正しいトークンを与える
    assert @user.authenticated?(:remember, @user.remember_token)
  end

  # set, del password_reset_digest
  test 'パスワード再設定関連属性が適切に保存されるか' do
    # set
    assert @user.password_reset_digest.nil?
    assert @user.password_reset_sent_at.nil?
    @user.set_password_reset_digest
    @user.reload
    assert_not @user.password_reset_digest.nil?
    assert_not @user.password_reset_sent_at.nil?
    # del
    @user.del_password_reset_digest
    assert @user.password_reset_digest.nil?
    assert @user.password_reset_sent_at.nil?
  end

  # send_password_reset_email
  # 統合テストで確認

  # password_reset_expired?
  test 'パスワード再設定期間を適切に判断する' do
    # 期限切れでない => false
    @user.update(password_reset_sent_at: 1.hour.ago)
    assert_not @user.password_reset_expired?
    # 期限切れ => true
    @user.update(password_reset_sent_at: 3.hours.ago)
    assert @user.password_reset_expired?
  end

  # feed
  test 'feed' do
    default = users(:default)
    hanako  = users(:hanako)
    jiro    = users(:jiro)

    # フォローしているユーザの投稿を確認
    hanako.microposts.each do |micropost|
      assert default.feed.include?(micropost)
    end
    # 自分自身の投稿を確認
    default.microposts.each do |micropost|
      assert default.feed.include?(micropost)
    end
    # フォローしていないユーザの投稿を確認
    jiro.microposts.each do |micropost|
      assert_not default.feed.include?(micropost)
    end
  end

  # follow/unfollow/following?/followers
  test 'フォロー,フォロー解除,フォローの存在性' do
    default = users(:default)
    jiro = users(:jiro)

    assert_not default.following?(jiro)
    # default -> jiro
    default.follow(jiro)
    # defaultはjiroをフォローしている
    assert default.following?(jiro)
    # jiroはdefaultにフォローされている
    assert jiro.followers.include?(default)
    # defaultはjiroのフォローを解除　-> defaultとjiroの関係がなくなる
    default.unfollow(jiro)
    assert_not default.following?(jiro)
    assert_not jiro.followers.include?(default)
  end

  # already_likes?
  test 'その投稿がすでにお気に入りに登録してあるか判断する' do
    @user.save
    micropost = microposts(:hanako_fourth)
    assert_not @user.already_likes?(micropost)
    params = { micropost: micropost }
    like = @user.likes.build(params)
    assert like.valid?
    like.save
    assert @user.already_likes?(micropost)
  end

  # notify_to, destroy_notification
  test 'notificationsレコードの作成と削除(follow)' do
    sender   = users(:default)
    receiver = users(:jiro)
    #  フォロー (defaultがjiroをフォローし、その通知レコードを作成する)
    sender.follow(receiver)
    follow_instance = Relationship.find_by(follower_id: sender.id,
                                           followed_id: receiver.id)
    assert_difference 'Notification.count', 1 do
      sender.notify_to(receiver, follow_instance)
    end
    # レコード検証
    record = receiver.receive_notifications.last
    assert_equal record.sender_id, sender.id
    assert_equal record.receiver_id, receiver.id
    assert_equal record.m_name, 'Relationship'
    assert_equal record.r_id, follow_instance.id
    # 削除する
    assert_difference 'Notification.count', -1 do
      sender.destroy_notification(receiver, follow_instance)
    end
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
  end

  test 'notificationsレコードの作成と削除(comment)' do
    sender   = users(:default)
    receiver = users(:jiro)
    #  コメント (defaultがjiroの投稿にコメントし、その通知レコードを作成する)
    micropost = microposts(:jiro_first)
    comment_instance = sender.comments.build(micropost_id: micropost.id,
                                             context:   'Good')
    comment_instance.save
    comment_instance.reload
    assert_difference 'Notification.count', 1 do
      sender.notify_to(receiver, comment_instance)
    end
    # レコード検証
    record = receiver.receive_notifications.last
    assert_equal record.sender_id, sender.id
    assert_equal record.receiver_id, receiver.id
    assert_equal record.m_name, 'Comment'
    assert_equal record.r_id, comment_instance.id
    # 削除する
    assert_difference 'Notification.count', -1 do
      sender.destroy_notification(receiver, comment_instance)
    end
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
  end

  test 'notificationsレコードの作成と削除(Like)' do
    sender   = users(:default)
    receiver = users(:jiro)
    #  コメント (defaultがjiroの投稿をお気に入りにし、その通知レコードを作成する)
    micropost = microposts(:jiro_first)
    like_instance = sender.likes.build(micropost_id: micropost.id,
                                       user_id:      receiver.id)
    like_instance.save
    like_instance.reload
    assert_difference 'Notification.count', 1 do
      sender.notify_to(receiver, like_instance)
    end
    # レコード検証
    record = receiver.receive_notifications.last
    assert_equal record.sender_id, sender.id
    assert_equal record.receiver_id, receiver.id
    assert_equal record.m_name, 'Like'
    assert_equal record.r_id, like_instance.id
    # 削除する
    assert_difference 'Notification.count', -1 do
      sender.destroy_notification(receiver, like_instance)
    end
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
  end

  test '通知の送信者と受信者が同じ場合には、レコードを作成しないこと(フォロー)' do
    sender   = users(:default)
    receiver = users(:default)
    #  フォロー (defaultが自身をフォローするが、通知レコードは作成されない)
    sender.follow(receiver)
    follow_instance = Relationship.find_by(follower_id: sender.id,
                                           followed_id: receiver.id)
    assert_no_difference 'Notification.count' do
      sender.notify_to(receiver, follow_instance)
    end
    # 削除する(メソッドをコールしたときに例外が発生しないこと)
    assert_nothing_raised do
      sender.destroy_notification(receiver, follow_instance)
    end
  end

  test '通知の送信者と受信者が同じ場合には、レコードを作成しないこと(コメント)' do
    sender = users(:default)
    #  コメント (defaultが自身の投稿にコメントするが、通知レコードは作成されない)
    micropost = microposts(:first)
    comment_instance = sender.comments.build(micropost_id: micropost.id,
                                             context:   'Good')
    comment_instance.save
    comment_instance.reload
    assert_no_difference 'Notification.count' do
      sender.notify_to(comment_instance.micropost.user, comment_instance)
    end
    # 削除する
    assert_nothing_raised do
      sender.destroy_notification(comment_instance.micropost.user,
                                  comment_instance)
    end
  end

  test '通知の送信者と受信者が同じ場合には、レコードを作成しないこと(Like)' do
    sender   = users(:default)
    receiver = users(:default)
    #  コメント (defaultが自身の投稿をお気に入りするが、その通知レコードは作成されない)
    micropost = microposts(:first)
    like_instance = sender.likes.build(micropost_id: micropost.id,
                                       user_id:      receiver.id)
    like_instance.save
    like_instance.reload
    assert_no_difference 'Notification.count' do
      sender.notify_to(like_instance.micropost.user, like_instance)
    end
    # 削除する
    assert_nothing_raised do
      sender.destroy_notification(like_instance.micropost.user, like_instance)
    end
  end

  # new_notifications_count, new_notifications?
  test 'そのユーザに対する新規通知有無と合計数の検証' do
    receiver = users(:hanako)
    assert receiver.new_notifications?
    assert_equal 3, receiver.new_notifications_count
    # 一番最初の新規通知を確認済にする => 新規通知数は1つ減る
    assert_difference 'receiver.new_notifications_count', -1 do
      receiver.receive_notifications.first.update(confirmed: true)
    end
  end

  # 関係性検証
  #-----------------------
  # micropost
  test 'ユーザのアカウントが削除された場合、紐づいている投稿も削除されること' do
    @user.save
    picture = fixture_file_upload(Rails.root.join('test',
                                                  'fixtures',
                                                  'files',
                                                  'sample1.jpg'))

    micropost = @user.microposts.build(content: 'hello',
                                       picture: picture)
    micropost.save
    assert_equal 1, @user.microposts.count
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
    assert_nil Micropost.find_by(id: micropost.id)
  end

  # relationship
  test 'ユーザのアカウントが削除された場合、紐づいている関係も削除されること(passive)' do
    # passive(フォローした側が削除された)
    default = users(:default)
    jiro    = users(:jiro)

    default.follow(jiro)
    assert_difference 'jiro.followers.count', -1 do
      default.destroy
    end
    assert_nil jiro.passive_relationships.find_by(id: default.id)
  end

  test 'ユーザのアカウントが削除された場合、紐づいている関係も削除されること(active)' do
    # active(フォローされた側が削除された)
    default = users(:default)
    jiro    = users(:jiro)
    default.follow(jiro)
    assert_difference 'default.following.count', -1 do
      jiro.destroy
    end
    assert_nil default.active_relationships.find_by(id: jiro.id)
  end

  # comment
  test 'ユーザが削除された場合、紐づいているコメントも削除されること' do
    hanako  = users(:hanako)
    comment = microposts(:first).comments
    assert_difference 'comment.count', -2 do
      hanako.destroy
    end
  end

  # like
  test 'ユーザが削除された場合、紐づいているお気に入りも削除されること' do
    user = users(:default)
    assert_difference 'Like.count', -3 do
      user.destroy
    end
  end

  # notification
  test 'ユーザが削除された場合、紐づいている通知も削除されること' do
    # notice: 送信側、受信側ともに外部キーで紐づいているため、
    #         どちらかが消滅した段階で、関連する通知はすべて削除される。
    #         ex.
    #         送信者(sender)：A   受信者(receiver)：B
    #         送信者(sender)：C   受信者(receiver)：A
    #         ユーザAを削除した段階で、上記２レコードも消滅する
    user = users(:default)
    assert_difference 'Notification.count', -4 do
      user.destroy
    end
  end
end
