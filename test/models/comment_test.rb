require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # セットアップ
  #=======================
  def setup
    @user      = users(:default)
    @micropost = microposts(:hanako_first)
    params = { micropost: @micropost, context: 'Lorem ipsum' }
    @comment = @user.comments.build(params)
  end

  # テスト
  #=======================

  test 'オブジェクトの有効性の確認' do
    assert @comment.valid?
  end

  # 各要素のバリデーション検証
  #-----------------------

  test '投稿idはnilで有ってはならない' do
    @comment.micropost_id = nil
    assert_not @comment.valid?
  end

  test 'ユーザidはnilで有ってはならない' do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test 'コメント本文は空で有ってはならない' do
    @comment.context = ' '
    assert_not @comment.valid?
  end

  test 'コメント本文は150文字以内とする' do
    @comment.context = 'a' * 151
    assert_not @comment.valid?
  end

  test 'コメントの取得順は日付の降順(新->古)とする' do
    assert_equal comments(:most_recent), Comment.first
  end

  # メソッド
  #-----------------------

  test 'その投稿に対するコメントと、コメントしたユーザの情報を返す' do
    user = users(:hanako)
    micropost = microposts(:first)
    context = 'Hello I am a boy.'
    params  = { micropost: micropost, context: context }
    new_comment = @user.comments.build(params)
    assert new_comment.valid?
    assert_difference 'Comment.count', 1 do
      new_comment.save
    end
    micropost.comments.each do |comment|
      comment.user    = user
      comment.context = context
    end
  end

  # to_jp_date_format
  # テストするときは、日付を変えること
  # test '作成日付を日本形式で返すこと' do
  #   comment = comments(:one)
  #   assert_equal '2019年9月21日', comment.created_at_jp_format
  # end
end
