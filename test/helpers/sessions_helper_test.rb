require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test 'loginメソッド' do
    login(@user)
    assert_equal @user.id, session[:user_id]
  end

  test 'rememberメソッド' do
    remember(@user)
    # user.remember_digestはモデル単体で検証
    assert_not cookies['user_id'].nil?
    assert_not cookies['remember_token'].nil?
    assert_equal @user.remember_token, cookies['remember_token']
    assert_equal @user.id, cookies.signed['user_id']
  end

  test 'forgotメソッド' do
    remember(@user)
    forgot(@user)
    # user.del_remember_digestはモデル単体で検証
    assert cookies['user_id'].nil?
    assert cookies['remember_token'].nil?
  end

  test 'current_userメソッド(ログインなし && cookieに情報なし)' do
    assert_not t_login?
    assert current_user.nil?
    assert_not t_login?
  end

  test 'current_userメソッド(ログイン && cookieに情報なし)' do
    assert_not t_login?
    t_login(@user)
    assert_equal @user, current_user
    assert t_login?
  end

  test 'current_userメソッド(ログイン && cookieに情報あり)' do
    assert_not t_login?
    t_login(@user)
    remember(@user)
    assert_equal @user, current_user
    assert t_login?
  end

  test 'current_userメソッド(ログインなし && cookieに情報あり)' do
    assert_not t_login?
    remember(@user)
    assert_equal @user, current_user
    assert t_login?
  end

  test 'current_userメソッド(ログインなし && cookieに情報あり(トークンが不正))' do
    assert_not t_login?
    remember(@user)
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert current_user.nil?
    assert_not t_login?
  end

  test 'current_userメソッド(ログインなし && cookieに情報あり(ユーザidが不正))' do
    assert_not t_login?
    remember(@user)
    cookies.signed[:user_id] = 99_999_999_999
    assert current_user.nil?
    assert_not t_login?
  end
end
