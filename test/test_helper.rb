ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
# minitest reportersを使う
require 'minitest/reporters'
Minitest::Reporters.use!
# carrierwave
require 'carrierwave_helper'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for
  # all tests in alphabetical order.
  fixtures :all

  # ログインする(テスト用)
  def t_login(user)
    session[:user_id] = user.id
  end

  # ログイン中かどうかを返す(テスト用)
  def t_login?
    !session[:user_id].nil?
  end
end

class ActionDispatch::IntegrationTest
  # ログインする(結合テスト用)
  def it_login(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { user_name: user.user_name,
                                          password: password,
                                          remember_me: remember_me } }
  end

  # gender要素を文字列にする
  def it_gender_to_string(gender_id)
    case gender_id
    when 0
      '男'
    when 1
      '女'
    when 9
      '答えたくない'
    else
      ''
    end
  end
end
