require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test '無効な情報でログイン' do
    # ユーザネーム,パスワードが空
    get login_path
    invalid_params = { user_name: '', password: '' }
    invalid_login_test(invalid_params)
    get root_path
    assert flash.empty?
    # ユーザネームに対するパスワードが違う
    user = users(:default)
    invalid_params = { user_name: user.user_name, password: 'foobar' }
    invalid_login_test(invalid_params)
  end

  test '有効な情報でログイン & ログアウト' do
    get login_path
    post login_path, params: { session: { user_name: @user.user_name,
                                        password: 'password' } }
    assert_redirected_to user_path(@user)
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert t_login?
    # ログアウト
    delete logout_path
    assert_not t_login?
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test 'ログイン->ログアウト->ログアウトとした時にエラーにならないこと' do
    get login_path
    post login_path, params: { session: { user_name: @user.user_name,
                                          password: 'password' } }
    assert t_login?
    delete logout_path
    assert_not t_login?
    delete logout_path
  end

  test 'remember_meがONの場合、ログイン時にユーザ情報をcookiesに保存する' do
    assert cookies['user_id'].nil?
    assert cookies['remember_token'].nil?
    it_login(@user, remember_me: '1')
    assert_not cookies['user_id'].nil?
    assert_not cookies['remember_token'].nil?
  end

  test 'remember_meがOFFの場合、ログイン時にユーザ情報をcookiesに保存しない' do
    assert cookies['user_id'].nil?
    assert cookies['remember_token'].nil?
    it_login(@user, remember_me: '0')
    assert cookies['user_id'].nil?
    assert cookies['remember_token'].nil?
  end

  # private
  #=======================
  def invalid_login_test(params)
    post login_path, params: { session: params }
    assert_template 'sessions/new'
    assert_not flash.empty?
  end
end
