require 'test_helper'

class NotificationsTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @sender   = users(:default)
    @receiver = users(:jiro)
    @micropost = microposts(:jiro_first)
  end

  # テスト
  #=======================

  # notice: 通知テストの上でAjaxの使用有無は関係ないので、本テストにおいては
  #         デフォルト動作であるAjaxを使用する検証とする。
  test 'フォローしたことを相手に通知する/フォロー解除したら通知を削除する' do
    it_login(@sender)
    get user_path(@receiver)
    # フォローする
    assert_difference '@sender.following.count', 1 do
      assert_difference '@receiver.receive_notifications.count', 1 do
        post relationships_path, xhr: true,
                                params: { followed_id: @receiver.id }
      end
    end
    # 通知の確認
    # レコード状態の確認
    record = Notification.first
    assert_equal @sender.id,           record.sender_id
    assert_equal @receiver.id,         record.receiver_id
    assert_equal 'Relationship',       record.m_name
    assert_equal Relationship.last.id, record.r_id
    # jiroでログインして、適切に情報が表示されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 1.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_match @sender.full_name,      response.body
    assert_match @sender.user_name,      response.body
    assert_select 'img.gravatar'
    assert_match 'あなたをフォローしました', response.body
    # 新着通知の表示件数が0になること
    get root_path
    assert_equal 0, @receiver.new_notifications_count
    assert_select 'span.new-notification-counter', text: 0.to_s
    #
    # フォロー解除
    #
    it_login(@sender)
    get user_path(@receiver)
    # フォロー解除する
    assert_difference '@sender.following.count', -1 do
      assert_difference '@receiver.receive_notifications.count', -1 do
        delete relationship_path(Relationship.last), xhr: true
      end
    end
    # 通知レコード削除確認
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
    # jiroでログインして、内容が削除されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 0.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_no_match @sender.full_name,      response.body
    assert_no_match @sender.user_name,      response.body
    assert_select 'img.gravatar', count: 0
    assert_no_match 'あなたをフォローしました', response.body
    assert_select 'div.card', count: 0
  end

  test 'コメントしたことを相手に通知する/コメントを削除したら通知を削除する' do
    it_login(@sender)
    get user_path(@receiver)
    # コメントする
    assert_difference 'Comment.count', 1 do
      assert_difference '@receiver.receive_notifications.count', 1 do
        post comments_path, params: { comment: { micropost_id: @micropost.id,
                                                 context:      'nice!' } }
      end
    end
    # 通知の確認
    # レコード状態の確認
    record = Notification.first
    assert_equal @sender.id,           record.sender_id
    assert_equal @receiver.id,         record.receiver_id
    assert_equal 'Comment',            record.m_name
    assert_equal Comment.first.id,     record.r_id
    # jiroでログインして、適切に情報が表示されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 1.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_match @sender.full_name,           response.body
    assert_match @sender.user_name,           response.body
    assert_select 'img.gravatar'
    assert_match 'あなたの投稿にコメントをしました', response.body
    assert_select 'a[href=?]', micropost_path(@micropost)
    # 新着通知の表示件数が0になること
    get root_path
    assert_equal 0, @receiver.new_notifications_count
    assert_select 'span.new-notification-counter', text: 0.to_s
    #
    # コメント削除
    #
    it_login(@sender)
    get user_path(@receiver)
    # コメント削除
    assert_difference 'Comment.count', -1 do
      assert_difference '@receiver.receive_notifications.count', -1 do
        delete comment_path(@micropost.comments.first)
      end
    end
    # 通知レコード削除確認
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
    # jiroでログインして、内容が削除されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 0.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_no_match @sender.full_name,            response.body
    assert_no_match @sender.user_name,            response.body
    assert_select 'img.gravatar', count: 0
    assert_no_match 'あなたの投稿にコメントをしました', response.body
    assert_select 'a[href=?]', micropost_path(@micropost), count: 0
    assert_select 'div.card', count: 0
  end

  test 'お気に入りしたことを相手に通知する/お気に入りを削除したら通知を削除する' do
    it_login(@sender)
    get user_path(@receiver)
    # お気に入りにする
    assert_difference 'Like.count', 1 do
      assert_difference '@receiver.receive_notifications.count', 1 do
        post likes_path, xhr: true, params: { micropost_id: @micropost.id }
      end
    end
    # 通知の確認
    # レコード状態の確認
    record = Notification.first
    assert_equal @sender.id,           record.sender_id
    assert_equal @receiver.id,         record.receiver_id
    assert_equal 'Like',               record.m_name
    assert_equal Like.first.id,        record.r_id
    # jiroでログインして、適切に情報が表示されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 1.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_match @sender.full_name,              response.body
    assert_match @sender.user_name,              response.body
    assert_select 'img.gravatar'
    assert_match 'あなたの投稿をお気に入りにしました', response.body
    assert_select 'a[href=?]', micropost_path(@micropost)
    # 新着通知の表示件数が0になること
    get root_path
    assert_equal 0, @receiver.new_notifications_count
    assert_select 'span.new-notification-counter', text: 0.to_s
    #
    # お気に入り削除
    #
    it_login(@sender)
    get user_path(@receiver)
    # お気に入り削除
    assert_difference 'Like.count', -1 do
      assert_difference '@receiver.receive_notifications.count', -1 do
        delete like_path(Like.first), xhr: true
      end
    end
    # 通知レコード削除確認
    assert_raises ActiveRecord::RecordNotFound do
      record.reload
    end
    # jiroでログインして、内容が削除されているか確認
    it_login(@receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 0.to_s
    get notifications_path
    assert_template 'notifications/index'
    assert_no_match @sender.full_name,            response.body
    assert_no_match @sender.user_name,            response.body
    assert_select 'img.gravatar', count: 0
    assert_no_match 'あなたの投稿をお気に入りにしました', response.body
    assert_select 'a[href=?]', micropost_path(@micropost), count: 0
    assert_select 'div.card', count: 0
  end

  test '新規通知数整合性チェック' do
    # テストケース
    # (1) 最初、hanakoは新規通知を3つ持っている(fixture上)
    # (2) ログインしたときに、ヘッダに新規(3)と表示されている
    # (3) 通知一覧ページを閲覧し、ルートパスへ移動する
    # (4) 3のアクションの際に、ここまでの新規通知は0件になり、その旨表示される
    # (5) defaultが花子の投稿にコメントをつける
    # (6) defaultが花子の投稿をお気に入りにする
    # (7) hanakoで再度ログインし、ヘッダの新規通知数が2件であることを確認する
    # (8) 通知一覧ページを閲覧し、新規通知 + 確認済通知の計5件のデータが表示されている
    #     ことを確認する
    #
    receiver = users(:hanako)
    it_login(receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 3.to_s
    get notifications_path
    assert_select 'div.card', count: 3
    receiver.receive_notifications.each do |notification|
      assert_match notification.sender.full_name, response.body
      assert_match notification.sender.user_name, response.body
    end
    get root_path
    assert_select 'span.new-notification-counter', text: 0.to_s
    # コメントをつける
    it_login(@sender)
    micropost = microposts(:hanako_fourth)
    post comments_path, params: { comment: { micropost_id: micropost.id,
                                             context:      'nice!' } }
    # お気に入りにする
    post likes_path, xhr: true, params: { micropost_id: micropost.id }
    # hanakoで再度ログインする
    it_login(receiver)
    get root_path
    assert_select 'span.new-notification-counter', text: 2.to_s
    # 通知一覧へ移動する
    get notifications_path
    assert_select 'div.card', count: 5
    receiver.receive_notifications.each do |notification|
      assert_match notification.sender.full_name, response.body
      assert_match notification.sender.user_name, response.body
    end
  end
end
