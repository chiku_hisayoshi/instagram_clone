require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test 'ユーザプロフィールページが適切に表示されているか(非ログイン状態)' do
    get user_path(@user)
    assert_match @user.full_name, response.body
    assert_match @user.user_name, response.body
    assert_match "#{@user.microposts.count}件", response.body
    @user.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url, response.body if micropost.picture?
      assert_select 'a.micropost-delete-link', count: 0
      assert_select 'a[href=?]', micropost_path(micropost), text: '投稿を削除',
                                                            count: 0
      assert_select 'a[href=?]', micropost_path(micropost), text: '写真個別ページ'
    end
  end

  test 'ユーザプロフィールページが適切に表示されているか(ログイン状態)' do
    it_login(@user)
    get user_path(@user)
    assert t_login?
    assert_match @user.full_name, response.body
    assert_match @user.user_name, response.body
    assert_match "#{@user.microposts.count}件", response.body
    @user.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url, response.body if micropost.picture?
      assert_select 'a.micropost-delete-link'
      assert_select 'a[href=?]', micropost_path(micropost), count: 2
    end
  end

  test 'ユーザプロフィールページが適切に表示されているか(ログイン状態&&他人のページ)' do
    it_login(@user)
    other = users(:hanako)
    get user_path(other)
    assert t_login?
    assert_match @user.full_name, response.body
    assert_match @user.user_name, response.body
    assert_match other.full_name, response.body
    assert_match other.user_name, response.body
    assert_match "#{other.microposts.count}件", response.body
    other.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url, response.body if micropost.picture?
      assert_select 'a.micropost-delete-link', count: 0
      assert_select 'a[href=?]', micropost_path(micropost), text: '投稿を削除',
                                                            count: 0
      assert_select 'a[href=?]', micropost_path(micropost), text: '写真個別ページ'
    end
  end
end
