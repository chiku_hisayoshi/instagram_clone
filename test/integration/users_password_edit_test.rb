require 'test_helper'

class UsersPasswordEditTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test '無効な情報でパスワードを更新する' do
    it_login(@user)
    get edit_password_path(@user)
    digest = @user.password_digest
    # 全て空の状態
    invalid_params = { old_password: '',
                       password: '',
                       password_confirmation: '' }
    invalid_edit_password_test(invalid_params, digest)
    # 現在のパスワードのみ入力した状態(間違ったパスワード)
    invalid_params[:old_password] = 'foobar'
    invalid_edit_password_test(invalid_params, digest)
    # 現在のパスワードのみ入力した状態(正しいパスワード)
    invalid_params[:old_password] = 'password'
    invalid_edit_password_test(invalid_params, digest)
    # 現在のパスワードは正しいが、新しいパスワードと確認が不一致
    invalid_params[:password]              = 'newpassword'
    invalid_params[:password_confirmation] = 'nnnpassword'
    patch edit_password_path(@user), params: { user: invalid_params }
    assert_equal digest, @user.reload.password_digest
    # HTML, CSS検証
    assert_template 'users/edit_password'
    assert_select 'div.field_with_errors'
    assert_select 'div.alert-danger'
    user = assigns(:user)
    assert_select '.alert-danger li', count: user.errors.count
    user.errors.full_messages.each do |err|
      assert_match err, response.body
    end
  end

  test '有効な情報でパスワードを更新する' do
    it_login(@user)
    get edit_password_path(@user)
    digest = @user.password_digest
    valid_params = { old_password: 'password',
                     password: 'new_password',
                     password_confirmation: 'new_password' }
    patch edit_password_path(@user), params: { user: valid_params }
    assert_not flash['success'].empty?
    assert_redirected_to @user
    assert_not_equal digest, @user.reload.password_digest
  end

  # private
  #=======================
  private

  def invalid_edit_password_test(params, digest)
    patch edit_password_path(@user), params: { user: params }
    assert_equal digest, @user.reload.password_digest
    assert_not flash['danger'].empty?
    assert_template 'users/edit_password'
  end
end
