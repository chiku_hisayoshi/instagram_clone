require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup; end

  # テスト
  #=======================
  test '無効なサインアップ' do
    get signup_path
    assert_select 'form[action=?]', signup_path
    # 全ての入力内容が不正
    invalid_params = { full_name: '', user_name: '',
                      password: 'foo',
                      password_confirmation: 'bar',
                      tos: '0' }
    invalid_signup_test(invalid_params)
    # パスワード不一致
    invalid_params = { full_name: 'foo', user_name: 'bar',
                      password: 'foobar',
                      password_confirmation: 'barfoo',
                      tos: '1' }
    invalid_signup_test(invalid_params)
    # 利用規約にチェックを入れていない
    invalid_params = { full_name: 'foo', user_name: 'bar',
                      password: 'foobar',
                      password_confirmation: 'foobar',
                      tos: '0' }
    invalid_signup_test(invalid_params)
  end

  test 'すでに存在するユーザネームでサインアップ' do
    get signup_path
    valid_params = { full_name: 'foobar', user_name: 'foobar',
                     password: 'foobar',
                     password_confirmation: 'foobar',
                     tos: '1' }
    assert_difference 'User.count', 1 do
      post signup_path, params: { user: valid_params }
    end
    # すでに存在するユーザ名でpostする
    invalid_params = { full_name: 'hoge', user_name: 'foobar',
                       password: 'hogehoge',
                       password_confirmation: 'hogehoge',
                       tos: '1' }
    invalid_signup_test(invalid_params)
  end

  test '有効な内容でサインアップ' do
    get signup_path
    valid_params = { full_name: 'foobar', user_name: 'foobar',
                     password: 'foobar',
                     password_confirmation: 'foobar',
                     tos: '1' }
    assert_difference 'User.count', 1 do
      post signup_path, params: { user: valid_params }
    end
    user = assigns(:user)
    assert_redirected_to user_path(user)
    follow_redirect!
    assert t_login?
    assert_template 'users/show'
    assert_not flash['success'].empty?
    assert_select 'div.alert-success'
    assert_match user.user_name, response.body
  end

  # private
  #-----------------------
  private

  def invalid_signup_test(params)
    assert_no_difference 'User.count' do
      post signup_path, params: { user: params }
    end
    assert_template 'users/new'

    # HTML, CSS検証
    assert_select 'div.field_with_errors'
    assert_select 'div.alert-danger'
    user = assigns(:user)
    assert_select '.alert-danger li', count: user.errors.count
    user.errors.full_messages.each do |err|
      assert_match err, response.body
    end
  end
end
