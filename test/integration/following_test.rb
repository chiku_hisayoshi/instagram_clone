require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user  = users(:default)
    @other = users(:jiro)
    it_login(@user)
  end

  # テスト
  #=======================

  test 'followingページ(非Ajax)' do
    get following_user_path(@user)
    assert_not @user.following.empty?
    assert_match @user.following.count.to_s, response.body
    assert_select 'img.gravatar', count: @user.following.count
    assert_select 'h4',
                  text: "#{@user.full_name}@#{@user.user_name}がフォロー中"
    @user.following.each do |user|
      assert_select 'a[href=?]', user_path(user)
      assert_match  user.full_name, response.body
      assert_match  user.user_name, response.body
      assert_match  user.introduction, response.body
    end
  end

  test 'followersページ(非Ajax)' do
    get followers_user_path(@user)
    assert_not @user.followers.empty?
    assert_match @user.followers.count.to_s, response.body
    assert_select 'img.gravatar', count: @user.followers.count
    assert_select 'h4',
                  text: "#{@user.full_name}@#{@user.user_name}をフォロー中"
    @user.followers.each do |user|
      assert_select 'a[href=?]', user_path(user)
      assert_match  user.full_name, response.body
      assert_match  user.user_name, response.body
      assert_match  user.introduction, response.body
    end
  end

  test 'フォローの検証(非Ajax)' do
    get user_path(@other)
    assert_not @user.following.include?(@other)
    assert_select 'input[type=submit][value=?]', 'フォローする'
    assert_difference '@user.following.count', 1 do
      post relationships_path, params: { followed_id: @other.id }
    end
    assert_redirected_to user_path(@other)
    follow_redirect!
    expect = "フォロワ #{@other.reload.followers.count}人"
    assert_select  'span#followers', text: expect
    assert_select 'input[type=submit][value=?]', 'フォロー中'
    get user_path(@user)
    expect = "フォロー #{@user.reload.following.count}人"
    assert_select  'span#following', text: expect
  end

  test 'フォロー解除の検証(非Ajax)' do
    other = users(:hanako)
    relationship = relationships(:default_following_hanako)
    get user_path(other)
    assert @user.following.include?(other)
    assert_select 'input[type=submit][value=?]', 'フォロー中'
    assert_difference '@user.following.count', -1 do
      delete relationship_path(relationship)
    end
    assert_redirected_to user_path(other)
    follow_redirect!
    expect = "フォロワ #{other.reload.followers.count}人"
    assert_select  'span#followers', text: expect
    assert_select 'input[type=submit][value=?]', 'フォローする'
    get user_path(@user)
    expect = "フォロー #{@user.reload.following.count}人"
    assert_select  'span#following', text: expect
  end

  test 'followingページ(Ajax)' do
    get following_user_path(@user), xhr: true
    assert_not @user.following.empty?
    assert_match @user.following.count.to_s, response.body
    @user.following.each do |user|
      assert_match  user.id.to_s,       response.body
      assert_match  user.full_name,     response.body
      assert_match  user.user_name,     response.body
      assert_match  user.introduction,  response.body
    end
  end

  test 'followersページ(Ajax)' do
    get followers_user_path(@user), xhr: true
    assert_not @user.followers.empty?
    assert_match @user.followers.count.to_s, response.body
    @user.followers.each do |user|
      assert_match  user.id.to_s,       response.body
      assert_match  user.full_name,     response.body
      assert_match  user.user_name,     response.body
      assert_match  user.introduction,  response.body
    end
  end

  test 'フォローの検証(Ajax)' do
    get user_path(@other)
    assert_not @user.following.include?(@other)
    assert_select 'input[type=submit][value=?]', 'フォローする'
    assert_difference '@user.following.count', 1 do
      post relationships_path, xhr: true, params: { followed_id: @other.id }
    end
    expect = "フォロワ #{@other.reload.followers.count}人"
    assert_match expect, response.body
    assert_match 'フォロー中', response.body
    get user_path(@user)
    expect = "フォロー #{@user.reload.following.count}人"
    assert_select  'span#following', text: expect
  end

  test 'フォロー解除の検証(Ajax)' do
    other = users(:hanako)
    relationship = relationships(:default_following_hanako)
    get user_path(other)
    assert @user.following.include?(other)
    assert_select 'input[type=submit][value=?]', 'フォロー中'
    assert_difference '@user.following.count', -1 do
      delete relationship_path(relationship), xhr: true
    end
    expect = "フォロワ #{other.reload.followers.count}人"
    assert_match expect, response.body
    assert_match 'フォローする', response.body
    get user_path(@user)
    expect = "フォロー #{@user.reload.following.count}人"
    assert_select  'span#following', text: expect
  end
end
