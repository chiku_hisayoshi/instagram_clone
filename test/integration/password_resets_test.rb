require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:default)
  end

  # テスト
  #=======================
  test '無効な情報でパスワード再設定' do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # ユーザネーム,メールアドレス共に空
    invalid_params = { user_name: '', email: '' }
    invalid_password_reset_test(invalid_params)
    # ユーザネーム(存在しない), メールアドレス(不正)
    invalid_params = { user_name: 'foobarbarbar', email: 'fooooo@bar' }
    invalid_password_reset_test(invalid_params)
    # ユーザネーム(存在する), メールアドレス(不正)
    invalid_password_reset_test(invalid_params)
    assert_template 'password_resets/new'
    # ユーザネーム(不正), メールアドレス(正当)
    invalid_params = { user_name: 'foobarbarbar', email: 'foo@bar.com' }
    invalid_password_reset_test(invalid_params)
    # ユーザネーム(正当), メールアドレス(空)
    invalid_params = { user_name: @user.user_name, email: '' }
    invalid_password_reset_test(invalid_params)
  end

  test '有効な情報でパスワード再設定 but URLが不正' do
    valid_params = { user_name: @user.user_name, email: 'foobar@bar.com' }
    post password_resets_path, params: { password_reset: valid_params }
    assert_not_equal @user.password_reset_digest,
                     @user.reload.password_reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_url
    #
    # メールが送信され、記載されてあるURLをクリックしたものと仮定
    #
    user = assigns(:user)
    # ユーザネーム、トークンが無効
    valid_password_reset_but_invalid_url('aaa', 'aaa')
    # ユーザネームは正当であるが、トークンが無効
    valid_password_reset_but_invalid_url('aaa', user.user_name)
    # ユーザネームは無効であるが、トークンが有効
    valid_password_reset_but_invalid_url(user.password_reset_token, 'hoge')
  end

  test '有効な情報でパスワード再設定 and URLが正当 but 再設定内容が不正' do
    valid_params = { user_name: @user.user_name, email: 'foobar@bar.com' }
    post password_resets_path, params: { password_reset: valid_params }
    assert_redirected_to root_url
    #
    # メールが送信され、記載されてあるURLをクリックしたものと仮定
    #
    user = assigns(:user)
    # ユーザネーム, トークン共に正当
    get edit_password_reset_path(user.password_reset_token,
                                 user_name: user.user_name)
    assert_template 'password_resets/edit'
    assert_select 'input[name=user_name][type=hidden][value=?]', user.user_name
    assert_match  user.user_name, response.body.encode
    #
    # update検証
    #
    user_tmp = user.dup
    # パスワード、確認が空
    option = { user: user, password: '', password_confirmation: '' }
    valid_url_but_invalid_input_content(option)
    # パスワードのみ入力し、確認が空
    user = user_tmp.dup
    option = { user: user, password: 'hogehoge', password_confirmation: '' }
    valid_url_but_invalid_input_content(option)
    # パスワードが空、確認のみ入力
    user = user_tmp.dup
    option = { user: user, password: '', password_confirmation: 'hogehoge' }
    valid_url_but_invalid_input_content(option)
    # パスワード、確認に入力されているが、要件に合わないもの
    user = user_tmp.dup
    option = { user: user, password: 'hoge', password_confirmation: 'hoge' }
    valid_url_but_invalid_input_content(option)
    # パスワード、確認に入力されているが、一致しない
    user = user_tmp.dup
    option = { user: user, password: 'hogehoge',
              password_confirmation: 'foobar' }
    valid_url_but_invalid_input_content(option)
  end

  test 'パスワード設定期限が切れている場合' do
    get new_password_reset_path
    valid_params = { user_name: @user.user_name, email: 'foobar@bar.com' }
    post password_resets_path, params: { password_reset: valid_params }
    user = assigns(:user)
    user.update(password_reset_sent_at: 3.hours.ago)
    patch password_reset_path(user.password_reset_token),
          params: { user_name: user.user_name,
                    user: { password:              'password',
                            password_confirmation: 'password' } }
    assert_not flash.empty?
    assert_redirected_to new_password_reset_url
    follow_redirect!
    assert_match '期間が切れています', response.body.encode
  end

  test '全て成功した場合' do
    # 有効な情報でメールを送信
    get new_password_reset_path
    valid_params = { user_name: @user.user_name, email: 'foobar@bar.com' }
    post password_resets_path, params: { password_reset: valid_params }
    # ユーザネーム, トークン共に正当
    user = assigns(:user)
    get edit_password_reset_path(user.password_reset_token,
                                 user_name: user.user_name)
    # パスワード、確認ともに正当な内容
    patch password_reset_path(user.password_reset_token),
          params: { user_name: user.user_name,
                    user: { password:              'password',
                            password_confirmation: 'password' } }
    assert t_login?
    assert_not flash[:success].empty?
    assert_redirected_to user
    assert_not_equal user.password_digest, user.reload.password_digest
    assert user.reload.password_reset_digest.nil?
  end

  # private
  #=======================
  def invalid_password_reset_test(params)
    post password_resets_path, params: { password_reset: params }
    assert_not flash.empty?
    assert_template 'password_resets/new'
  end

  def valid_password_reset_but_invalid_url(token, user_name)
    get edit_password_reset_path(token, user_name: user_name)
    assert_not flash['danger'].empty?
    assert_redirected_to root_url
  end

  def valid_url_but_invalid_input_content(option = {})
    user                  = option[:user]
    password              = option[:password]
    password_confirmation = option[:password_confirmation]

    patch password_reset_path(user.password_reset_token),
          params: { user_name: user.user_name,
                    user: { password:              password,
                            password_confirmation: password_confirmation } }
    assert_template 'edit'
    # HTML, CSS検証
    user = assigns(:user)
    assert_select 'div.field_with_errors'
    assert_select 'div.alert-danger'
    assert_select '.alert-danger li', count: user.errors.count
    user.errors.full_messages.each do |err|
      assert_match err, response.body
    end
  end
end
