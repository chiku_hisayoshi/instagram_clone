require 'test_helper'

class MicropostsIntegrationTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test '写真を投稿し、削除する' do
    it_login(@user)
    get new_micropost_path
    # 無効な送信
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: '' } }
    end
    assert_template 'microposts/new'
    # HTML, CSS検証
    assert_select 'div.field_with_errors'
    assert_select 'div.alert-danger'
    micropost = assigns(:micropost)
    assert_select '.alert-danger li', count: micropost.errors.count
    micropost.errors.full_messages.each do |err|
      assert_match err, response.body
    end
    # 有効な送信
    picture = fixture_file_upload('../fixtures/sample1.jpg', 'image/jpg')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: 'Hello',
                                                   picture: picture } }
    end
    micropost = assigns(:micropost)
    assert_redirected_to micropost_path(micropost)
    follow_redirect!
    assert_match micropost.content, response.body
    assert_select 'img'
    assert_match micropost.picture.url, response.body
    assert_select 'p', text: micropost.content
    assert_select 'a[href=?]', micropost_path(micropost)
    # 投稿の削除
    first_micropost = @user.microposts.paginate(page: 1, per_page: 15).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    assert_raises ActiveRecord::RecordNotFound do
      micropost.reload
    end
    assert_not flash.empty?
    assert_response :redirect
  end

  test '他人の写真投稿は、削除することができない' do
    it_login(@user)
    hanako = users(:hanako)
    hanako_first_micropost = hanako.microposts
                                   .paginate(page: 1, per_page: 15).first
    assert_no_difference 'Micropost.count' do
      delete micropost_path(hanako_first_micropost)
    end
    assert_redirected_to root_url
  end

  test '画面に表示している投稿数カウントの確認' do
    it_login(@user)
    micropost_count = "#{@user.microposts.count}件"
    get root_path
    assert_match micropost_count, response.body
    get user_path(@user)
    assert_match micropost_count, response.body
    #  投稿を追加する
    picture = fixture_file_upload('../fixtures/sample1.jpg', 'image/jpg')
    post microposts_path, params: { micropost: { content: 'Hello',
                                                 picture: picture } }
    @user.reload
    micropost_count = "#{@user.microposts.count}件"
    get root_path
    assert_match micropost_count, response.body
    get user_path(@user)
    assert_match micropost_count, response.body
    # 投稿を削除する
    micropost = @user.microposts.paginate(page: 1, per_page: 15).first
    delete micropost_path(micropost)
    @user.reload
    micropost_count = "#{@user.microposts.count}件"
    get root_path
    assert_match micropost_count, response.body
    get user_path(@user)
    assert_match micropost_count, response.body
  end

  test 'トップページとユーザ個別ページに表示されている投稿が適切か' do
    # 非ログイン状態
    # トップページには投稿は表示されない
    # ユーザ個別ページ(自分)
    get user_path(@user)
    @user.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url if micropost.picture?
    end

    # ユーザ個別ページ(他人)
    other = users(:hanako)
    get user_path(other)
    other.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url if micropost.picture?
    end

    # ログイン状態
    it_login(@user)
    # トップページ
    get root_path
    @user.feed.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url if micropost.picture?
    end
    # ユーザ一覧ページ
    get user_path(@user)
    @user.microposts.paginate(page: 1, per_page: 15).each do |micropost|
      assert_match micropost.content, response.body
      assert_match micropost.picture.url if micropost.picture?
    end
  end
end
