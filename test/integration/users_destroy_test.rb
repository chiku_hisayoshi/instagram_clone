require 'test_helper'

class UsersDestroyTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test 'ユーザを削除する' do
    it_login(@user)
    get edit_user_path(@user)
    assert_select 'a[href=?]', user_path(@user), text: 'アカウント削除'
    assert_select 'a[data-method=?]', 'delete'
    assert_difference 'User.count', -1 do
      delete user_path(@user)
    end
    assert_redirected_to root_url
    assert session['user_id'].nil?
    assert cookies['user_id'].empty?
    assert cookies['remember_token'].empty?
    assert_raises ActiveRecord::RecordNotFound do
      @user.reload
    end
    follow_redirect!
    assert_template 'static_pages/home'
  end
end
