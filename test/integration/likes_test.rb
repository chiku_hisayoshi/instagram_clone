require 'test_helper'

class LikesTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================

  test 'お気に入り登録する(非Ajax)' do
    it_login(@user)
    # 準備
    picture = fixture_file_upload('../fixtures/sample1.jpg', 'image/jpg')
    post microposts_path, params: { micropost: { content: 'Hello',
                                                 picture: picture } }
    micropost = assigns(:micropost)
    # ここから本番
    get micropost_path(micropost)
    assert_select "div#likes-form-#{micropost.id}"
    # この時点ではお気に入り登録していないので、likeが表示される
    assert_select "form#form-like-#{micropost.id}"
    # お気に入り送信
    assert_difference 'Like.count', 1 do
      post likes_path, params: { micropost_id: micropost.id }
    end
    assert_response :redirect
    follow_redirect!
    assert_select "form#form-like-#{micropost.id}", count: 0
    # お気に入りにしたのでunlikeが表示される
    assert_select "div#likes-form-#{micropost.id} a"
    # POSTを２重送信する => エラーを表示し、リダイレクト
    assert_no_difference 'Like.count' do
      post likes_path, params: { micropost_id: micropost.id }
    end
    assert_not flash[:danger].empty?
    assert_response :redirect
    # お気に入りを削除する
    like = @user.likes.find_by(micropost_id: micropost.id)
    assert_difference 'Like.count', -1 do
      delete like_path(like)
    end
    assert_response :redirect
    get micropost_path(micropost)
    # お気に入りを削除したので、likeが表示される
    assert_select "form#form-like-#{micropost.id}"
    # DELETEを２重送信する => リダイレクトのみ(フェールセーフ)
    assert_no_difference 'Like.count' do
      delete like_path(like)
    end
    assert_response :redirect
  end

  test 'お気に入り登録する(Ajax)' do
    it_login(@user)
    # 準備
    picture = fixture_file_upload('../fixtures/sample1.jpg', 'image/jpg')
    post microposts_path, params: { micropost: { content: 'Hello',
                                                 picture: picture } }
    micropost = assigns(:micropost)
    # ここから本番
    get micropost_path(micropost)
    assert_select "div#likes-form-#{micropost.id}"
    # この時点ではお気に入り登録していないので、likeが表示される
    assert_select "form#form-like-#{micropost.id}"
    # お気に入り送信
    assert_difference 'Like.count', 1 do
      post likes_path, xhr: true, params: { micropost_id: micropost.id }
    end
    get micropost_path(micropost)
    assert_select "form#form-like-#{micropost.id}", count: 0
    # お気に入りにしたのでunlikeが表示される
    assert_select "div#likes-form-#{micropost.id} a"
    # POSTを２重送信する => エラーを表示し、リダイレクト
    assert_no_difference 'Like.count' do
      post likes_path, xhr: true, params: { micropost_id: micropost.id }
    end
    assert_not flash[:danger].empty?
    # お気に入りを削除する
    like = @user.likes.find_by(micropost_id: micropost.id)
    assert_difference 'Like.count', -1 do
      delete like_path(like), xhr: true
    end
    get micropost_path(micropost)
    # お気に入りを削除したので、likeが表示される
    assert_select "form#form-like-#{micropost.id}"
    # DELETEを２重送信する => なにもしない
    assert_no_difference 'Like.count' do
      delete like_path(like), xhr: true
    end
  end
end
