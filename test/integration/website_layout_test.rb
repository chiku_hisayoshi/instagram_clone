require 'test_helper'

class WebsiteLayoutTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  # application_helperの検証用
  include ApplicationHelper
  def setup
    @base_title = 'Instagram-clone'
  end

  # テスト
  #=======================
  test 'full_page_titleの検証' do
    assert ApplicationHelper
    # 引数なし
    assert_equal @base_title, full_page_title
    # 引数あり
    arg = 'サインアップ'
    assert_equal "#{arg} - #{@base_title}", full_page_title(arg)
  end

  # トップページ
  #-----------------------
  test 'トップページレイアウト検証(非ログイン)' do
    get root_path
    assert_template 'static_pages/home'
    # タイトル
    assert_select 'title', @base_title
    # リンクテスト
    assert_select 'a[href=?]', signup_path
    assert_select 'a[href=?]', login_path
  end

  test 'トップページレイアウト検証(ログイン)' do
    user = users(:default)
    it_login(user)
    assert t_login?
    get root_path
    assert_template 'static_pages/home'
    # タイトル
    assert_select 'title', @base_title
    # リンク
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', edit_user_path(user)
    assert_select 'a[href=?]', user_path(user)
    assert_select 'a[href=?]', logout_path
    assert_select 'a[href=?]', signup_path, count: 0
    assert_select 'a[href=?]', login_path,  count: 0
    assert_select 'a[href=?]', new_micropost_path
    assert_select 'div.pagination'
    assert_select 'a[href=?]', following_user_path(user)
    assert_select 'a[href=?]', followers_user_path(user)
    assert_match  user.introduction, response.body
    assert_match  user.website, response.body
  end

  # サインアップページ
  #-----------------------
  test 'サインアップページレイアウト検証' do
    get signup_path
    assert_template 'users/new'
    # タイトル
    expected = "サインアップ - #{@base_title}"
    assert_select 'title', expected
  end

  # ログインページ
  #-----------------------
  test 'ログインページのレイアウト検証' do
    get login_path
    assert_template 'sessions/new'
    # タイトル
    expected = "ログイン - #{@base_title}"
    assert_select 'title', expected
  end

  # 利用規約ページ
  #-----------------------
  test '利用規約ページのレイアウト検証' do
    get tos_path
    assert_template 'static_pages/tos'
    # タイトル
    expected = "利用規約 - #{@base_title}"
    assert_select 'title', expected
  end

  # プロフィール編集ページ
  #-----------------------
  test 'プロフィール編集ページのレイアウト検証' do
    user = users(:default)
    it_login(user)
    get edit_user_path(user)
    assert_template 'users/edit'
    # タイトル
    expected = "プロフィール編集 - #{@base_title}"
    assert_select 'title', expected
  end

  # パスワード変更ページ
  #-----------------------
  test 'パスワード変更ページのレイアウト検証' do
    user = users(:default)
    it_login(user)
    get edit_password_path(user)
    assert_template 'users/edit_password'
    # タイトル
    expected = "パスワード変更 - #{@base_title}"
    assert_select 'title', expected
  end

  # パスワード再設定ページ(new)
  #-----------------------
  test 'パスワード再設定ページ(new)のレイアウト' do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # タイトル
    expected = "パスワード再設定 - #{@base_title}"
    assert_select 'title', expected
  end

  # パスワード再設定ページ(edit)
  #-----------------------
  test 'パスワード再設定ページ(edit)のレイアウト' do
    user = users(:default)
    valid_params = { user_name: user.user_name, email: 'foobar@bar.com' }
    post password_resets_path, params: { password_reset: valid_params }
    assert_redirected_to root_url
    #
    # メールが送信され、記載されてあるURLをクリックしたものと仮定
    #
    user = assigns(:user)
    # ユーザネーム, トークン共に正当
    get edit_password_reset_path(user.password_reset_token,
                                 user_name: user.user_name)
    assert_template 'password_resets/edit'
    # タイトル
    expected = "パスワード再設定 - #{@base_title}"
    assert_select 'title', expected
  end

  # ユーザ一覧ページ
  #-----------------------
  test 'ユーザ一覧ページのレイアウト検証' do
    login_user = users(:default)
    it_login(login_user)
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    # タイトル
    expected = "ユーザ一覧 - #{@base_title}"
    assert_select 'title', expected
    # 一覧
    User.paginate(page: 1, per_page: 15).each do |user|
      display_name = user.full_name + '@' + user.user_name
      assert_select 'span.user-info-name', text: display_name
      assert_select 'span.user-info-introduction', text: user.introduction
      assert_select 'a.user-info-link[href=?]', user_path(user)
    end
  end

  # ユーザ個別ページ
  #-----------------------
  test 'ユーザ個別ページのレイアウト検証(非ログイン)' do
    user = users(:default)
    get user_url(user)
    assert_template 'users/show'
    # タイトル
    title = user.user_name
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    # リンク
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', edit_user_path(user), count: 0
    assert_select 'a[href=?]', logout_path, count: 0
    assert_select 'a[href=?]', signup_path, text: 'サインアップ'
    assert_select 'a[href=?]', login_path,  text: 'ログイン'
    assert_select 'a[href=?]', new_micropost_path, count: 0
    assert_match  "#{user.full_name}@#{user.user_name}", response.body
    assert_select 'div.pagination'
    assert_select 'a[href=?]', following_user_path(user)
    assert_select 'a[href=?]', followers_user_path(user)
  end

  test 'ユーザ個別ページのレイアウト検証(ログイン)' do
    user = users(:default)
    it_login(user)
    assert t_login?
    get user_url(user)
    assert_template 'users/show'
    # タイトル
    title = user.user_name
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    # リンク
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', edit_user_path(user)
    assert_select 'a[href=?]', user_path(user),
                  text: "#{user.full_name}@#{user.user_name}"
    assert_select 'a[href=?]', logout_path
    assert_select 'a[href=?]', signup_path, count: 0
    assert_select 'a[href=?]', login_path,  count: 0
    assert_select 'a[href=?]', new_micropost_path
    assert_select 'div.pagination'
    assert_match  "#{user.full_name}@#{user.user_name}", response.body
  end

  # 写真投稿ページ
  #-----------------------
  test '写真アップロード画面のレイアウト検証' do
    # 非ログインはアクセスできない
    get new_micropost_path
    assert_redirected_to login_path
    # ログインしてアクセス
    user = users(:default)
    it_login(user)
    assert t_login?
    get new_micropost_path
    assert_template 'microposts/new'
    # タイトル
    title = '写真投稿'
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    assert_select 'textarea'
    assert_select 'input[type=file]'
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', user_path(user),
                  text: "#{user.full_name}@#{user.user_name}"
    assert_select 'a[href=?]', logout_path
  end

  # 写真個別ページ
  #-----------------------
  test '写真個別ページのレイアウト検証(非ログイン)' do
    micropost = microposts(:first)
    user = User.find_by(id: micropost.user_id)
    get micropost_path(micropost)
    assert_template 'microposts/show'
    # タイトル
    title = '写真個別ページ'
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', user_path(user), count: 1
    assert_select 'a[href=?]', logout_path, count: 0
    assert_select 'a[href=?]', signup_path, text: 'サインアップ'
    assert_select 'a[href=?]', login_path,  text: 'ログイン'
    # 画像の表示, 投稿者コメントは統合テストで確認
    # コメント
    # カウント
    assert_select 'h6', text: "コメント (#{micropost.comments.count})"
    # gravatar, コメントしたユーザ名、コメント、削除リンク(なし)
    micropost.comments.each do |comment|
      # アイコン
      assert_select 'img[alt=?]', comment.user.user_name
      # ユーザ名
      expected = "#{comment.user.full_name}@#{comment.user.user_name}"
      assert_select 'span', text: expected
      # コメント
      assert_match comment.context, response.body
      # 削除リンク(なし)
      assert_select 'a[href=?]', comment_path(comment), text: '削除', count: 0
    end
  end

  test '写真個別ページのレイアウト検証(ログイン)' do
    user = users(:default)
    it_login(user)
    assert t_login?
    micropost = microposts(:first)
    get micropost_path(micropost)
    assert_template 'microposts/show'
    # タイトル
    title = '写真個別ページ'
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    assert_select 'a[href=?]', root_path
    assert_select 'a[href=?]', user_path(user),
                  text: "#{user.full_name}@#{user.user_name}"
    assert_select 'a[href=?]', logout_path
    assert_select 'a[href=?]', signup_path, count: 0
    assert_select 'a[href=?]', login_path,  count: 0
    # 画像の表示, 投稿者コメントは統合テストで確認
    # コメント
    # カウント
    assert_select 'h6', text: "コメント (#{micropost.comments.count})"
    # gravatar, コメントしたユーザ名、コメント、削除リンク
    micropost.comments.each do |comment|
      # アイコン
      assert_select 'img[alt=?]', comment.user.user_name
      # ユーザ名
      expected = "#{comment.user.full_name}@#{comment.user.user_name}"
      assert_select 'span', text: expected
      # コメント
      assert_match comment.context, response.body
      # 削除リンク
      # ログインユーザであれば削除リンクを表示する,そうでないならば表示しない
      if comment.user == user
        assert_select 'a[href=?]', comment_path(comment), text: '削除'
      else
        assert_select 'a[href=?]', comment_path(comment), text: '削除', count: 0
      end
    end
  end

  # フォロー一覧(非Ajax)
  #-----------------------
  test 'フォロー一覧のレイアウト検証' do
    user = users(:default)
    it_login(user)
    get following_user_path(user)
    assert_template 'users/following'
    # タイトル
    title = 'フォロー一覧'
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    # ヘッダ
    assert_select 'header'
  end

  # フォロワ一覧(非Ajax)
  #-----------------------
  test 'フォロワ一覧のレイアウト検証' do
    user = users(:default)
    it_login(user)
    get followers_user_path(user)
    assert_template 'users/followers'
    # タイトル
    title = 'フォロワ一覧'
    expected = "#{title} - #{@base_title}"
    assert_select 'title', expected
    # ヘッダ
    assert_select 'header'
  end
end
