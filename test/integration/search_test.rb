require 'test_helper'

class SearchTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================

  test '検索キーワードを入力し、検索結果を表示する' do
    it_login(@user)
    get root_path
    # 検索フォームの有無チェック
    assert_select 'form#search-form[action=?]', search_items_path
    assert_select 'form#search-form input'
    assert_select 'form#search-form span'
    # 検索POSTを送信(無効-空白)
    post search_items_path, params: { keyword: '' }
    assert_not flash[:danger].empty?
    assert_redirected_to root_url
    # 検索POSTを送信(有効)
    post search_items_path, params: { keyword: '花子' }
    assert_redirected_to search_item_path('花子')
    # 結果の表示, 確認
    results = Micropost.where('content LIKE ?', '%花子%').paginate(page: 1,
                                                                  per_page: 15)
    follow_redirect!
    results.each do |micropost|
      assert_match micropost.content, response.body
    end
    assert_select 'div.micropost-card', count: results.count
  end
end
