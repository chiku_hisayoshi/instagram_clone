require 'test_helper'

class CommentsTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user      = users(:default)
    @micropost = microposts(:hanako_first)
  end

  # テスト
  #=======================

  test 'ユーザが投稿した画像にコメントし、削除する' do
    it_login(@user)
    get micropost_path(@micropost)
    # HTML要素
    assert_select 'form[action=?]', comments_path
    assert_select 'form[method=?]', 'post'
    assert_select 'input[type=hidden][value=?]', @micropost.id.to_s
    assert_select 'textarea#comment_context'
    # 無効な送信
    # コメントを空で送信
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { micropost_id: @micropost.id,
                                               context: '' } }
    end
    assert_not flash[:danger].empty?
    assert_redirected_to micropost_path(@micropost)
    # 150文字以上で送信
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { micropost_id: @micropost.id,
                                               context: 'a' * 151 } }
    end
    assert_not flash[:danger].empty?
    assert_redirected_to micropost_path(@micropost)
    # 有効な送信
    assert_difference 'Comment.count', 1 do
      post comments_path, params: { comment: { micropost_id: @micropost.id,
                                               context:      'nice!' } }
    end
    assert_not flash[:success].empty?
    assert_redirected_to micropost_path(@micropost)
    follow_redirect!
    # コメントが反映されているか
    comment = @micropost.comments.first
    assert_match  comment.context, response.body
    assert_select 'a>span',
                  text: "#{@user.full_name}@#{@user.user_name}"
    assert_select 'a[href=?]', user_path(@user), count: 2

    ## 削除
    assert_difference 'Comment.count', -1 do
      delete comment_path(comment)
    end
    assert_raises ActiveRecord::RecordNotFound do
      comment.reload
    end
    assert_not flash[:success].empty?
    assert_response :redirect
    follow_redirect!
    # コメントが表示されていない(削除)されたことを確認する
    get micropost_path(@micropost)
    assert_no_match comment.context, response.body
    assert_select 'a[href=?]', user_path(@user), count: 1
  end
end
