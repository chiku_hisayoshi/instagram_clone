require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================
  test '編集失敗時の検証' do
    it_login(@user)
    # 全ての要素が空であるとき
    get edit_user_path(@user)
    invalid_edit_params = get_edit_params
    invalid_edit_test(invalid_edit_params)
    # フルネームのみ入力された場合
    invalid_edit_params = get_edit_params(fu: @user.full_name)
    invalid_edit_test(invalid_edit_params)
    # ユーザネームのみ入力された場合
    invalid_edit_params = get_edit_params(us: @user.user_name)
    invalid_edit_test(invalid_edit_params)
    # フルネーム以外は入力された場合
    invalid_edit_params = get_edit_params(us: @user.user_name,
                                          we: 'http://foobar.com',
                                          intro: 'Hello I am boy.',
                                          em: 'foobar@bar.com',
                                          te: '09012345678',
                                          ge: 1)
    invalid_edit_test(invalid_edit_params)
  end

  test '編集成功時の検証' do
    it_login(@user)
    get edit_user_path(@user)
    full_name = 'hoge'
    user_name = 'hoge123'
    website = 'http://foo.bar'
    introduction = 'Hello I am boy.'
    email = 'foo@bar.com'
    tel = '09012345678'
    gender = '1'
    # 全ての要素が入力されている場合
    valid_edit_params = get_edit_params(fu: full_name,
                                        us: user_name,
                                        we: website,
                                        intro: introduction,
                                        em: email,
                                        te: tel,
                                        ge: gender)
    valid_edit_test(valid_edit_params)
    # 一部のみ入力している場合
    valid_edit_params = get_edit_params(fu: full_name,
                                       us: user_name,
                                       we: '',
                                       intro: introduction,
                                       em: email,
                                       te: '',
                                       ge: '0')
    valid_edit_test(valid_edit_params)
    valid_edit_params = get_edit_params(fu: full_name,
                                      us: user_name,
                                      we: website,
                                      intro: '',
                                      em: '',
                                      te: tel,
                                      ge: '9')
    valid_edit_test(valid_edit_params)
    # 性別が空
    valid_edit_params[:gender] = ''
    valid_edit_test(valid_edit_params)
  end

  test 'フレンドリーフォワーディング' do
    get edit_user_path(@user)
    it_login(@user)
    assert_redirected_to edit_user_path(@user)
  end

  # private
  #=======================
  private

  # notice: キーワード引数は、それぞれの要素の頭から２文字
  def get_edit_params(fu: '', us: '', we: '', intro: '', em: '', te: '', ge: '')
    { full_name: fu,
      user_name: us,
      website: we,
      introduction: intro,
      email: em,
      tel: te,
      gender: ge }
  end

  def invalid_edit_test(params)
    original_user = @user.dup
    patch user_path(@user), params: { user: params }
    assert_template 'users/edit'
    @user.reload
    assert_equal original_user.full_name, @user.full_name
    assert_equal original_user.user_name, @user.user_name
    assert_equal original_user.password_digest, @user.password_digest
    assert_nil @user.email
    assert_equal original_user.website, @user.website
    assert_equal original_user.introduction, @user.introduction
    assert_nil @user.tel
    assert_nil @user.gender

    # HTML, CSS検証
    assert_select 'div.field_with_errors'
    assert_select 'div.alert-danger'
    user = assigns(:user)
    assert_select '.alert-danger li', count: user.errors.count
    user.errors.full_messages.each do |err|
      assert_match err, response.body
    end
  end

  def valid_edit_test(params)
    patch user_path(@user), params: { user: params }
    assert @user.valid?
    assert_not flash.empty?
    assert_redirected_to user_path(@user)
    @user.reload
    assert_equal params[:full_name], @user.full_name
    assert_equal params[:user_name], @user.user_name
    assert_equal params[:website],   @user.website
    assert_equal params[:introduction], @user.introduction
    assert_equal params[:email],     @user.email
    assert_equal params[:tel],       @user.tel
    assert_equal params[:gender], @user.gender.to_s

    # HTML, CSS検証
    get edit_user_path(@user)
    assert_select 'input#user_full_name', value: @user.full_name
    assert_select 'input#user_user_name', value: @user.user_name
    assert_select 'input#user_website',   value: @user.website
    assert_select 'textarea#user_introduction', value: @user.introduction
    assert_select 'input#user_email',     value: @user.email
    assert_select 'input#user_tel',       value: @user.tel
    if @user.gender.blank?
      assert_select 'select#user_gender>option', count: 4
    else
      assert_select 'select#user_gender>option[selected="selected"]',
                    text: it_gender_to_string(@user.gender)
    end
  end
end
