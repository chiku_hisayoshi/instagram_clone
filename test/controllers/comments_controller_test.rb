require 'test_helper'

class CommentsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================

  test '非ログインユーザはコメントできない' do
    micropost = microposts(:hanako_first)
    get micropost_path(micropost)
    assert_no_difference 'Comment.count' do
      post comments_path, params: { comment: { micropost_id: micropost.id,
                                                    context: 'lorem ipsum' } }
    end
    assert_redirected_to login_url
  end

  test '非ログインユーザはコメントを削除することはできない' do
    micropost = microposts(:first)
    get micropost_path(micropost)
    comment = comments(:one)
    assert_no_difference 'Comment.count' do
      delete comment_path(comment)
    end
    assert_redirected_to login_url
  end

  test 'ログインユーザと異なるユーザのコメントを削除することはできない' do
    it_login(@user)
    assert t_login?
    micropost = microposts(:first)
    assert_not micropost.comments.include?(@user)
    comment = micropost.comments.first
    assert_no_difference 'Comment.count' do
      delete comment_path(comment)
    end
    assert_not flash.empty?
    assert_response :redirect
  end
end
