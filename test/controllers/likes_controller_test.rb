require 'test_helper'

class LikesControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
    @micropost = microposts(:fourth)
  end

  # テスト
  #=======================

  test '非ログインユーザはお気に入りをすることができない' do
    get micropost_path(@micropost)
    # アイコンがないこと
    assert_select 'div#likes_form', count: 0
    # 直接POSTしてもログインパスへリダイレクトされること
    assert_no_difference 'Like.count' do
      post likes_path, params: { micropost_id: @micropost.id }
    end
    assert_redirected_to login_path
    # 直接DELETEを送ってもログインパスへリダイレクトされること
    like = likes(:one)
    assert_no_difference 'Like.count' do
      delete like_path(like)
    end
    assert_redirected_to login_path
  end
end
