require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user   = users(:default)
    @hanako = users(:hanako)
  end

  # テスト
  #=======================
  test 'サインアップページの検証' do
    get signup_path
    assert_response :success
  end

  # beforeのテスト
  #-----------------------
  test '非ログインユーザがプロフィール変更ページにアクセスした場合リダイレクトする' do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '非ログインユーザがプロフィール更新patchリクエストした場合リダイレクトする' do
    patch user_path(@user), params: { user: { full_name: @user.full_name,
                                              user_name: @user.user_name } }
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '他人のプロフィール変更ページにアクセスした場合リダイレクトする' do
    it_login(@user)
    get edit_user_path(@hanako)
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test '他人のプロフィール更新patchリクエストした場合リダイレクトする' do
    it_login(@user)
    patch user_path(@hanako), params: { user: { full_name: @user.full_name,
                                                user_name: @user.user_name } }
    assert_not flash.empty?
    assert_redirected_to root_path
  end

  test '非ログインユーザがパスワード変更ページにアクセスした場合リダイレクトする' do
    get edit_password_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '非ログインユーザがパスワード変更patchリクエストした場合リダイレクトする' do
    patch edit_password_path(@user), params: { user: {
      old_password: 'password',
                                              password: 'newpassword',
                                 password_confirmation: 'newpassword'
    } }
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '他人のパスワード変更ページにアクセスした場合リダイレクトする' do
    it_login(@user)
    get edit_password_path(@hanako)
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test '他人のパスワード変更patchリクエストした場合リダイレクトする' do
    it_login(@user)
    patch edit_password_path(@hanako), params: { user: {
      old_password: 'password',
                                              password: 'newpassword',
                                 password_confirmation: 'newpassword'
    } }
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test '非ログインユーザがdeleteリクエストした場合リダイレクトする' do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '他人に対してdeleteリクエストした場合リダイレクトする' do
    it_login(@user)
    assert_no_difference 'User.count' do
      delete user_path(@hanako)
    end
    assert_not flash.empty?
    assert_redirected_to root_url
  end

  test '非ログインユーザがユーザ一覧ページにアクセスした場合リダイレクトする' do
    get users_url
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test '存在しないユーザのページにアクセスした場合は、ルートへリダイレクトする' do
    get 'http://www.example.com/users/110'
    assert_redirected_to root_url
  end

  # zantei: ログイン状態に関わらず参照できたほうがよさそうなので、検討する
  # test '非ログインの場合、ユーザがフォローしている一覧を参照できない' do
  #   get following_user_path(@user)
  #   assert_redirected_to login_path
  # end
  #
  # test '非ログインの場合、ユーザをフォローしている一覧を参照できない' do
  #   get followers_user_path(@user)
  #   assert_redirected_to login_path
  # end
end
