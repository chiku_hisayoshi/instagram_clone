require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup; end

  # テスト
  #=======================
  test 'ルート(ランディングページ)の検証' do
    # レスポンス
    get root_url
    assert_response :success
  end
end
