require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @micropost = microposts(:first)
  end

  # テスト
  #=======================
  test '非ログイン状態では、写真投稿ページにアクセスできない' do
    get new_micropost_path
    assert_redirected_to login_url
  end

  test '非ログイン状態では、写真を投稿できない' do
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: 'Lorem ipsum' } }
    end
    assert_redirected_to login_url
  end

  test '非ログイン状態では、写真を削除できない' do
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)
    end
    assert_redirected_to login_url
  end

  test 'ログイン中ユーザ以外の投稿は削除できない' do
    user = users(:default)
    it_login(user)
    micropost = microposts(:hanako_first)
    assert_no_difference 'Micropost.count' do
      delete micropost_path(micropost)
    end
    assert_redirected_to root_url
  end

  test '存在しない投稿のページにアクセスした場合は、ルートページにリダイレクトする' do
    user = users(:default)
    it_login(user)
    get 'http://www.example.com/microposts/999'
    assert_redirected_to root_url
  end
end
