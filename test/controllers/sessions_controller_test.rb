require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup; end

  # テスト
  #=======================
  test 'ログインページへ正常にアクセスできること' do
    get login_path
    assert_response :success
  end
end
