require 'test_helper'

class SearchItemsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================

  test '非ログインユーザは検索POSTを送信できない' do
    post search_items_path, params: { keyword: 'foobar' }
    assert_redirected_to login_url
  end

  test '非ログインユーザは検索結果を参照できない' do
    id = 'foobar'
    get search_item_url(id)
    assert_redirected_to login_url
  end

  test 'ヘッダ表示用名称の取得' do
    it_login(@user)
    id = 'hogehoge'
    get search_item_url(id)
    expected = "#{@user.full_name}@#{@user.user_name}"
    assert_match expected, response.body
  end
end
