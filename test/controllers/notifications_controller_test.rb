require 'test_helper'

class NotificationsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup
    @user = users(:default)
  end

  # テスト
  #=======================

  test '非ログインユーザは通知一覧を参照できない' do
    get notifications_path
    assert_redirected_to login_path
  end
end
