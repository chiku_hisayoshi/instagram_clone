require 'test_helper'

class RelationshipsControllerTest < ActionDispatch::IntegrationTest
  # セットアップ
  #=======================
  def setup; end

  # テスト
  #=======================

  test '非ログインユーザはリレーションシップを作成することはできない' do
    assert_no_difference 'Relationship.count' do
      post relationships_path
    end
    assert_redirected_to login_url
  end

  test '非ログインユーザはリレーションシップを削除することはできない' do
    relationship = relationships(:default_following_hanako)
    assert_no_difference 'Relationship.count' do
      delete relationship_path(relationship)
    end
    assert_redirected_to login_url
  end
end
