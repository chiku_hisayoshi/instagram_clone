require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  # セットアップ
  #=======================
  def setup; end

  # テスト
  #=======================
  test 'パスワード再設定メールの検証' do
    user = users(:default)
    user.password_reset_token = User.new_token
    user.email = 'foobar@bar.com'
    mail = UserMailer.password_reset(user)
    # メールテンプレートの検証
    assert_equal 'パスワードの再設定', mail.subject
    assert_equal [user.email],              mail.to
    assert_equal ['noreply@example.com'],   mail.from
    assert_match to_base64(user.user_name), mail.body.encoded
    # うまくいかないので、サーバログで直接確認
    # assert_match to_base64(user.password_reset_token), mail.body.encoded
  end

  # private
  #=======================

  # 文字列をBase64に変換する
  def to_base64(str)
    base64_str = Base64.strict_encode64(str)
    base64_str.include?('=') ? base64_str.delete('=').chop : base64_str.chop
  end
end
