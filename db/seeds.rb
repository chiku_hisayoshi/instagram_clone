# 必須ユーザ
User.create!( 
  full_name: "テストユーザ123",
  user_name: 'test',
  email:     "test@test.com",
  gender: 0,
  tel: '09000000000',
  introduction: Faker::Lorem.paragraph(sentence_count: 4..8),
  website: 'http://www.google.co.jp',
  password:              "testtest",
  password_confirmation: "testtest" )
  
# その他ユーザ
30.times do |n|
full_name =  Faker::Name.unique.name
email     =  Faker::Internet.unique.email
user_name =  "test_#{n+1}"
gender    = n % 2
tel       = '09000000000'
introduction =  Faker::Lorem.paragraph(sentence_count: 4..8)
website   = "http://www.#{SecureRandom.urlsafe_base64}.com"
password  =  'password'

User.create!( 
  full_name: full_name,
  user_name: user_name,
  email:     email,
  gender:    gender,
  tel:       tel,
  introduction: introduction,
  website:   website,
  password:              password,
  password_confirmation: password )
end

# 投稿
users = User.order(:created_at).take(5)
picture = open("#{Rails.root}/db/fixtures/sample1.jpg")
30.times do
  content = Faker::Lorem.paragraph(sentence_count: 4..8)
  users.each { |user| user.microposts.create!(content: content,
                                              picture: picture) }
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..20]
followers = users[3..30]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }