class RemoveIndexUserNameToUsers < ActiveRecord::Migration[5.2]
  def change
    remove_index :users, column: :user_name
  end
end
