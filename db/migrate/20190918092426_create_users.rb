class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :user_name
      t.string :email
      t.integer :gender
      t.string :tel
      t.text :introduction

      t.timestamps
    end
  end
end
