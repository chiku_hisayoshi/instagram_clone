class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.integer :sender_id
      t.integer :receiver_id
      t.string :m_name
      t.integer :r_id
      t.boolean :confirmed, default: false

      t.timestamps
    end
    add_index :notifications, :sender_id
    add_index :notifications, :receiver_id
    add_index :notifications, [:m_name, :r_id], unique: true
  end
end
