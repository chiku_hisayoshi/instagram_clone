class RemoveIndexFromNotifications < ActiveRecord::Migration[5.2]
  def change
    remove_index :notifications, column: [:m_name, :r_id]
  end
end
