Rails.application.routes.draw do
  # ルート
  #=======================
  root 'static_pages#home'

  # StaticPages
  #=======================
  get '/tos',            to: 'static_pages#tos'

  # Users
  #=======================
  resources :users do
    member do
      get :following, :followers
    end
  end
  get  '/signup',   to: 'users#new'
  post '/signup',   to: 'users#create'
  get  '/users/:id/edit_password',  to: 'users#edit_password', 
                                    as: 'edit_password'
  patch '/users/:id/edit_password', to: 'users#update_password'

  # Sessions
  #=======================
  get    '/login',  to: 'sessions#new'
  post   '/login',  to: 'sessions#create'
  delete 'logout',  to: 'sessions#destroy'

  # PasswordResets
  #=======================
  resources :password_resets,  only: %i[new create edit update]

  # Microposts
  #=======================
  resources :microposts, only: %i[new create destroy show]

  # Relationships
  #=======================
  resources :relationships, only: %i[create destroy]

  # Comments
  #=======================
  resources :comments,  only: %i[create destroy]

  # Likes
  #=======================
  resources :likes,  only: %i[create destroy]

  # Search_items
  #=======================
  get '/search/:id',        to: 'search_items#show', as: 'search_item'
  resources :search_items, only: %i[create]

  # Facebook
  #=======================
  get '/auth/facebook/callback',  to: 'sessions#facebook',
                                  as: 'facebook_auth_callback'

  # Notifications
  #=======================
  resources :notifications, only: %i[index]
end
