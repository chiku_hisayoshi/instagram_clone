Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV['FACEBOOK_KEY'], ENV['FACEBOOK_SECRET'],
  local:        'ja_JP',
  client_options: {
    site:          'https://graph.facebook.com/v4.0',
    authorize_url: "https://www.facebook.com/v4.0/dialog/oauth"
  }
end