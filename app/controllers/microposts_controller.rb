class MicropostsController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[new create destroy]
  # ログイン中ユーザかどうか判断
  before_action :correct_user,   only: %i[destroy]
  # ログイン中ユーザのユーザ名
  before_action :full_current_user_name, only: %i[new show]
  # 投稿の存在性確認
  before_action :check_micropost_exist, only: %i[show]

  # Action
  #=======================
  def show
    @micropost = Micropost.find_by(id: params[:id])
    @comments  = @micropost.comments
  end

  def new
    @micropost = current_user.microposts.build
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = t 'success.micropost_post'
      redirect_to micropost_path(@micropost)
    else
      render 'new'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = t 'success.micropost_delete'
    # redirect_back(fallback_location: root_url)
    redirect_to user_path(current_user)
  end

  # private
  #=======================
  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end

  def check_micropost_exist
    redirect_to root_url unless Micropost.find_by(id: params[:id])
  end
end
