class PasswordResetsController < ApplicationController
  # before
  #=======================
  before_action :get_user,         only: %i[edit update]
  before_action :valid_user,       only: %i[edit update]
  before_action :check_expiration, only: %i[edit update]

  # Action
  #=======================
  def new; end

  def create
    @user = User.find_by(user_name: params[:password_reset][:user_name])
    email = params[:password_reset][:email]
    if @user && email_valid?(email)
      @user.set_password_reset_digest
      @user.send_password_reset_email
      flash[:info] = t 'info.send_email'
      redirect_to root_url
    else
      flash[:danger] = t 'danger.no_user_or_invalid_email'
      render 'new'
    end
  end

  def edit; end

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, :blank)
      render 'edit'
    elsif @user.update(user_params)
      login @user
      @user.del_password_reset_digest
      flash[:success] = t 'success.password_reset'
      redirect_to @user
    else
      render 'edit'
    end
  end

  # private
  #=======================
  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  # メールアドレスが有効であるか判断する
  def email_valid?(email)
    return if email.blank?

    @user.email = email
    @user.valid?
  end

  # ユーザネームからユーザを検索する
  def get_user
    @user = User.find_by(user_name: params[:user_name])
  end

  # 正当なユーザであるか判断する
  def valid_user
    return if @user&.authenticated?(:password_reset, params[:id])

    flash[:danger] = t 'danger.unknown'
    redirect_to root_url
  end

  # 再設定期間の有効性確認
  def check_expiration
    return unless @user.password_reset_expired?

    flash[:danger] = t 'danger.password_reset_expired'
    redirect_to new_password_reset_url
  end
end
