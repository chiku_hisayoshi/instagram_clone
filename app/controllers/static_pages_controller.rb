class StaticPagesController < ApplicationController
  # Before
  #=======================
  # ログイン中ユーザのユーザ名
  before_action :full_current_user_name, only: %i[home]

  # Action
  #=======================

  def home
    return unless login?

    @user = current_user
    @micropost = @user.microposts.build
    @feed_items = @user.feed.paginate(page: params[:page], per_page: 15)
  end

  def tos; end
end
