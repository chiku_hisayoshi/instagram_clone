class SearchItemsController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[show create]
  # ログイン中ユーザのユーザ名
  before_action :full_current_user_name, only: %i[show]

  # Action
  #=======================
  def create
    keyword = params[:keyword]
    if keyword.blank?
      flash[:danger] = t 'danger.no_search_keyword'
      redirect_back(fallback_location: root_url)
    else
      redirect_to search_item_url(keyword)
    end
  end

  def show
    keyword = params[:id]
    @results = get_results('content LIKE ?', "%#{keyword}%")
  end

  # private
  #=======================
  private

  def get_results(predicate, keyword)
    Micropost.where(predicate, keyword).paginate(page: params[:page],
                                                 per_page: 15)
  end
end
