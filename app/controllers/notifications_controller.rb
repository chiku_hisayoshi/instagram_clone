class NotificationsController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[index]
  # ログイン中ユーザのユーザ名
  before_action :full_current_user_name, only: %i[index]

  # After
  #=======================
  # indexページを開いた時点で確認済とする
  after_action  :update_confirmed_to_true

  # Action
  #=======================
  def index
    @notifications = current_user.receive_notifications
                                 .paginate(page: params[:page], per_page: 15)
  end

  # private
  #=======================
  private

  def update_confirmed_to_true
    # Notice:
    # current_user.receive_notificationsからupdate_allを実行すると
    # デッドロックが発生するため、逐次処理で更新する
    Notification.update_confirmed_to_true(current_user)
  end
end
