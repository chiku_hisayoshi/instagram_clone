class ApplicationController < ActionController::Base
  include SessionsHelper

  # private
  #=======================
  private

  # ログインしているユーザであるか判断
  def logged_in_user
    return if login?

    store_location
    flash[:danger] = t 'danger.no_logged_in'
    redirect_to login_url
  end

  # ログイン中ユーザの表示名を返す
  def full_current_user_name
    @disp_name = "#{current_user.full_name}@#{current_user.user_name}" if login?
  end
end
