class LikesController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[create destroy]

  # action
  #=======================

  def create
    @micropost = Micropost.find_by(id: params[:micropost_id])
    like = current_user.likes.build(like_params)
    unless like.save
      flash[:danger] =
        "お気に入り登録に失敗しました。#{like.errors.full_messages.join(' ')}"
    end
    current_user.notify_to(like.micropost.user, like) if like.persisted?
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_url) }
      format.js
    end
  end

  def destroy
    like = Like.find_by(id: params[:id])
    if like.present?
      @micropost = like.micropost
      like.destroy
      current_user.destroy_notification(like.micropost.user, like)
    end
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_url) }
      format.js
    end
  end

  # private
  #=======================
  private

  def like_params
    params.permit(:micropost_id, :user_id)
  end
end
