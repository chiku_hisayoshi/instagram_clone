class SessionsController < ApplicationController
  # Action
  #=======================
  def new; end

  def create
    user = User.find_by(user_name: params[:session][:user_name])
    if user&.authenticate(params[:session][:password])
      # success
      login user
      params[:session][:remember_me] == '1' ? remember(user) : forgot(user)
      flash[:success] = t 'success.login'
      redirect_back_or user
    else
      # failure
      set_flash(user)
      render 'new'
    end
  end

  def destroy
    logout if login?
    flash[:success] = t 'success.logout'
    redirect_to root_url
  end

  def facebook
    user = User.find_or_create_from_auth(request.env['omniauth.auth'])
    if user&.persisted?
      login user
      flash[:success] = t 'success.login'
      redirect_back_or user
    else
      flash[:danger] =
        "#{t 'danger.unknown'} (#{user.errors.full_messages.join(' ')})"
      redirect_to root_path
    end
  end

  # private
  #=======================
  private

  def set_flash(user)
    # 「userがnil」or「パスワードが間違っている」のみ対象とする
    flash.now[:danger] = if user.nil?
                           t 'danger.no_user'
                         else
                           t 'danger.invalid_password'
                         end
  end
end
