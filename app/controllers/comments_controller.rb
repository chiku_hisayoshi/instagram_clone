class CommentsController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[create destroy]
  # ログイン中ユーザかどうか判断
  before_action :correct_user,   only: %i[destroy]

  # Action
  #=======================
  def create
    micropost = Micropost.find_by(id: params[:comment][:micropost_id])
    comment = current_user.comments.build(comment_params)
    if comment.save
      current_user.notify_to(micropost.user, comment)
      flash[:success] = 'コメントを投稿しました'
    else
      flash[:danger] =
        "コメント投稿に失敗しました。#{comment.errors.full_messages.join(' ')}"
    end
    redirect_to micropost_path(micropost)
  end

  def destroy
    @comment.destroy
    current_user.destroy_notification(@comment.micropost.user, @comment)
    flash[:success] = 'コメントを削除しました'
    redirect_back(fallback_location: root_url)
  end

  # private
  #=======================
  private

  def comment_params
    params.require(:comment).permit(:micropost_id, :context)
  end

  def correct_user
    @comment = current_user.comments.find_by(id: params[:id])
    return unless @comment.nil?

    flash[:danger] = 'このコメントは削除できません'
    redirect_back(fallback_location: root_url)
  end
end
