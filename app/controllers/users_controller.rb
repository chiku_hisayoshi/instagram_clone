class UsersController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[index edit update destroy
                                          edit_password update_password]
  # ログインユーザフィルタ
  before_action :correct_user,   only: %i[edit update destroy
                                          edit_password update_password]
  # ログイン中ユーザのユーザ名
  before_action :full_current_user_name, only: %i[show following followers]
  # ユーザの存在性確認
  before_action :check_user_exist, only: %i[show]

  # Action
  #=======================

  def index
    @users = User.paginate(page: params[:page], per_page: 15)
  end

  def show
    @user = User.find_by(id: params[:id])
    @microposts = @user.microposts.paginate(page: params[:page], per_page: 15)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    tos_state = tos_checked?(params[:user][:tos])
    if tos_state && @user.save
      # success
      login @user
      flash[:success] = t 'success.signup', user_name: @user.user_name
      redirect_to @user
    else
      # failure
      unless tos_state
        # バリデーションかけていないため明示的にエラーを追加する
        @user.errors.add(:tos, :not_checked)
      end
      render 'new'
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
  end

  def update
    @user = User.find_by(id: params[:id])
    if @user.update(user_params)
      # success
      flash[:success] = t 'success.prifile_update'
      redirect_to @user
    else
      # failure
      render 'edit'
    end
  end

  def destroy
    user = User.find_by(id: params[:id])
    logout if login?
    user.destroy
    flash[:success] = t 'success.account_destroy'
    redirect_to root_url
  end

  def edit_password
    @user = User.find_by(id: params[:id])
  end

  def update_password
    @user = User.find_by(id: params[:id])
    old_password = params[:user][:old_password]
    # notice: ・@userが存在しないことは通常ありえないが、念の為
    #         ・パスワードは空白を許容するが、明示的なパスワード変更においては空白での
    #           更新は禁止する(空白でもレコードは更新はされないが、ユーザに注意を促すため)
    if !password_nil_or_blank? && @user&.authenticate(old_password)
      # 新しいパスワードが空でない && ユーザが存在 && 古いパスワードでの認証に通った場合
      if @user.update(user_params)
        # 属性の更新に成功したとき => success
        flash[:success] = t 'success.update_password'
        redirect_to @user
      else
        # failure(属性の更新に失敗)
        render 'edit_password'
      end
    else
      # failure(ユーザが存在しない or 認証に失敗)
      flash.now[:danger] = set_flash
      render 'edit_password'
    end
  end

  def following
    @title   = 'フォロー一覧'
    @context = 'このユーザがフォローしているユーザはいません'
    user = User.find_by(id: params[:id])
    @users = user.following.paginate(page: params[:page], per_page: 15)
    respond_to do |format|
      format.html   # => following.html.erb
      format.js     # => following.js.erb
    end
  end

  def followers
    @title   = 'フォロワ一覧'
    @context = 'このユーザをフォローしているユーザはいません'
    user = User.find_by(id: params[:id])
    @users = user.followers.paginate(page: params[:page], per_page: 15)
    respond_to do |format|
      format.html   # => followers.html.erb
      format.js     # => followers.js.erb
    end
  end

  # private
  #=======================
  private

  def user_params
    params.require(:user).permit(:full_name,
                                 :user_name,
                                 :password,
                                 :password_confirmation,
                                 :website,
                                 :introduction,
                                 :email,
                                 :tel,
                                 :gender)
  end

  # 利用規約をチェックしているか
  def tos_checked?(param)
    return false if param.nil? || param == '0'

    true
  end

  # 正しいユーザかどうか確認する
  def correct_user
    user = User.find_by(id: params[:id])
    redirect_to root_url unless current_user?(user)
  end

  # 入力されたパスワードが空でないか判断する
  def password_nil_or_blank?
    (params[:user][:password].nil? ||
     params[:user][:password_confirmation].nil?) ||
      (params[:user][:password].blank? ||
       params[:user][:password_confirmation].blank?)
  end

  # フラッシュメッセージを決定する
  def set_flash
    if password_nil_or_blank?
      t 'danger.password_blank'
    elsif !@user.authenticate(params[:user][:old_password])
      t 'danger.failure_auth'
    elsif @user.nil?
      t 'danger.no_user'
    else
      t 'danger.unknown'
    end
  end

  # 存在しないユーザのページにアクセスした場合はルートへリダイレクトする
  def check_user_exist
    redirect_to root_url unless User.find_by(id: params[:id])
  end
end
