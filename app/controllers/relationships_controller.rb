class RelationshipsController < ApplicationController
  # Before
  #=======================
  # ログイン済フィルタ
  before_action :logged_in_user, only: %i[create destroy]

  # Action
  #=======================

  def create
    @user = User.find_by(id: params[:followed_id])
    current_user.follow(@user)
    current_user.notify_to(@user,
                           Relationship.find_by(follower_id: current_user.id,
                                                followed_id: @user.id))
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    relation = Relationship.find_by(id: params[:id])
    @user = relation.followed
    current_user.unfollow(@user)
    current_user.destroy_notification(@user, relation)
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end
end
