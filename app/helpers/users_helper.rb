module UsersHelper
  # gravatarの画像を返す
  def gravatar_for(user, size: 80)
    gravatar_id =
      if user.email.nil?
        'dummy123@123.xxx'
      else
        Digest::MD5.hexdigest(user.email.downcase)
      end
    gravatar_url =
      "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.user_name,
                            class: 'gravatar rounded-circle img-thumbnail')
  end
end
