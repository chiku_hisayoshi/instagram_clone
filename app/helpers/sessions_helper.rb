module SessionsHelper
  # 引数のユーザでログインする
  def login(user)
    session[:user_id] = user.id
  end

  # ログイン中かどうかを返す(ログイン中->true)
  def login?
    !current_user.nil?
  end

  # 引数のユーザがログインしているユーザかどうか判断する
  def current_user?(user)
    current_user == user
  end

  # cookiesにログイン中ユーザの情報を格納する
  def remember(user)
    user.set_remember_digest
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # cookiesにログイン中ユーザの情報を削除する
  def forgot(user)
    user.del_remember_digest
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # ログイン中ユーザを返す
  def current_user
    if (user_id = session[:user_id])
      # 1. 明示的にログインした場合(loginメソッドをコールしている場合)
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      # 2. 明示的なログインはないが、cookieに情報がある場合
      user = User.find_by(id: user_id)
      if user&.authenticated?(:remember, cookies[:remember_token])
        # 3. 2の場合&&ユーザが存在する&&クッキーに保存してあるトークンで認証を
        #    パスした場合 -> 正規のユーザとしてログインを許可する
        login user
        @current_user = user
      end
    end
  end

  # ログアウト
  def logout
    forgot(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # 記憶したURLにリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # アクセスしたURLを保存する
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
