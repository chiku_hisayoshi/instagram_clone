module ApplicationHelper
  # ページタイトルの補完
  def full_page_title(title = '')
    base_title = 'Instagram-clone'
    full_title = "#{title} - #{base_title}"
    title.empty? ? base_title : full_title
  end
end
