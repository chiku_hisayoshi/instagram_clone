module NotificationsHelper
  # 通知表示のためのメッセージを返す
  def get_notification_message(notification)
    model_name = notification.m_name
    case model_name
    when Relationship.to_s
      'あなたをフォローしました'
    when Comment.to_s
      'あなたの投稿にコメントをしました'
    when Like.to_s
      'あなたの投稿をお気に入りにしました'
    end
  end

  # 通知件数が100件以上の場合は、99+と表示させる
  def notification_count_limit(count)
    if count > 99
      '99+'
    else
      count.to_s
    end
  end
end
