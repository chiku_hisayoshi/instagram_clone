class Like < ApplicationRecord
  # 関連
  #=======================
  belongs_to :user
  belongs_to :micropost

  # バリデーション
  #=======================
  default_scope -> { order(created_at: :desc) }
  validates :micropost_id, presence: true
  validates :user_id,      presence: true,
                           uniqueness: { scope: [:micropost_id] }
end
