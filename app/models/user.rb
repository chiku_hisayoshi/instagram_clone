class User < ApplicationRecord
  # 関連
  #=======================
  # 写真投稿
  has_many :microposts, dependent: :destroy
  # 能動的な関係 ユーザ -> 他ユーザ
  has_many :active_relationships,  class_name: 'Relationship',
                                   foreign_key: 'follower_id',
                                   dependent:   :destroy
  # 受動的な関係 ユーザ <- 他ユーザ
  has_many :passive_relationships, class_name: 'Relationship',
                                   foreign_key: 'followed_id',
                                   dependent:   :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  # コメント
  has_many :comments, dependent: :destroy
  # お気に入り
  has_many :likes,    dependent: :destroy
  # 通知
  has_many :send_notifications,     class_name:  'Notification',
                                    foreign_key: 'sender_id',
                                    dependent:   :destroy
  has_many :receive_notifications,  class_name:  'Notification',
                                    foreign_key: 'receiver_id',
                                    dependent:   :destroy
  has_many :notification_senders,   through: :receive_notifications,
                                    source:  :sender

  # before
  #=======================
  before_save :attr_to_downcase,
              :gender_to_integer

  # attribute
  #=======================
  attr_accessor :tos,            # 利用規約チェック有無
                :remember_token, # cookie保存用トークン
                :password_reset_token # パスワード再設定用トークン

  # 定数
  #=======================
  # ユーザネーム用フォーマット
  VALID_USERNAME_REGEX =
    /\A(\w{1,50})\z/i.freeze

  # メールアドレス用フォーマット
  VALID_EMAIL_REGEX =
    /\A([\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+)\z/i.freeze

  # 電話番号用フォーマット
  # notice: 最低限のチェックのみ, 固定電話許容, ハイフン抜き
  VALID_TEL_REGEX = /\A(\d{8,17})\z/.freeze

  # パスワード用フォーマット
  VALID_PASSWORD_REGEX =
    /\A([\w\!\#\$\%\&\+\-\=\?\@\_\|\*]{6,})\z/.freeze

  # ウェブサイト用フォーマット
  VALID_WEBSITE_REGEX =
    %r{\A(https?\://[\w/\:\%\#\$\&\?\(\)~\.=\+\-]+)\z}i.freeze

  # バリデーション
  #=======================
  # NOTICE
  # ユーザ登録時の情報(フルネーム, ユーザネーム, パスワード)以外はnil,空白を許容する

  # フルネーム
  validates :full_name, presence: true,          # 空欄を許容しない
                        length: { maximum: 50 }, # 50文字まで
                        allow_nil: false         # nilを許容しない
  # ユーザネーム
  validates :user_name, presence: true, # 空欄を許容しない
                        format: { with: VALID_USERNAME_REGEX }, # フォーマットを指定する
                        uniqueness: { case_sensitive: false },  # 一意性を確保する
                        allow_nil: false # nilを許容しない
  # メールアドレス
  validates :email,     length: { maximum: 255 },              # 255文字まで
                        format: { with: VALID_EMAIL_REGEX },   # フォーマットを指定する
                        uniqueness: { case_sensitive: false }, # 一意性を確保する
                        allow_blank: true                      # nil,空白を許容
  # 性別(念の為)
  validates :gender,    length: { maximum: 1 }, # 1文字まで
                        allow_blank: true # nil,空白を許容
  # 電話番号
  validates :tel,       format: { with: VALID_TEL_REGEX }, # フォーマットを指定する
                        allow_blank: true                  # nil,空白を許容
  # 自己紹介
  validates :introduction, length: { maximum: 300 }, # 300文字まで
                           allow_blank: true         # nil,空白を許容
  # ウェブサイト
  validates :website,   length: { maximum: 2083 }, # IEにおける最大長が2083文字のため
                        allow_blank: true,         # nil許容
                        format: { with: VALID_WEBSITE_REGEX } # フォーマットを指定する
  # パスワード
  has_secure_password
  validates :password,  presence: true,                         # 空白を許容しない
                        format: { with: VALID_PASSWORD_REGEX }, # フォーマットを指定する
                        allow_nil: true

  # 特異メソッド
  #=======================
  class << self
    # ダイジェストを作成する
    def digest(string)
      cost =
        if ActiveModel::SecurePassword.min_cost
          BCrypt::Engine::MIN_COST
        else
          BCrypt::Engine.cost
        end
      BCrypt::Password.create(string, cost: cost)
    end

    # トークンの生成
    def new_token
      SecureRandom.urlsafe_base64
    end

    # facebookでのログイン用
    def find_or_create_from_auth(auth)
      uid       = auth['uid']
      provider  = auth['provider']
      full_name = auth['info']['name']
      user_name = "#{uid}_#{provider}" # user_nameの一意性を担保するため
      email     = auth['info']['email']
      password  = User.new_token

      unless user ||= User.find_by(uid: uid, provider: provider)
        user = User.create(
          uid:        uid,
          provider:   provider,
          full_name:  full_name,
          user_name:  user_name,
          email:      email,
          password:              password,
          password_confirmation: password
        )
      end

      user
    end
  end

  # インスタンスメソッド
  #=======================
  # 記憶ダイジェストの生成と保存
  def set_remember_digest
    self.remember_token = User.new_token
    digest = User.digest(remember_token)
    # noteice: バリデーションの回避は意図的
    update_attribute(:remember_digest, digest)
  end

  # 記憶ダイジェストの削除して保存
  def del_remember_digest
    self.remember_token = nil
    update_attribute(:remember_digest, nil)
  end

  # トークンとダイジェストの認証
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?

    BCrypt::Password.new(digest).is_password?(token)
  end

  # パスワード再設定属性の更新
  def set_password_reset_digest
    self.password_reset_token = User.new_token
    update(password_reset_digest: User.digest(password_reset_token))
    update(password_reset_sent_at: Time.zone.now)
  end

  # パスワード再設定属性の削除
  def del_password_reset_digest
    update(password_reset_digest:  nil)
    update(password_reset_sent_at: nil)
  end

  # パスワード再設定メールの送信
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # パスワード再設定期間の確認
  def password_reset_expired?
    password_reset_sent_at < 2.hours.ago
  end

  # feed
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
  end

  # ユーザをフォローする
  def follow(user)
    following << user
  end

  # ユーザのフォローを解除する
  def unfollow(user)
    active_relationships.find_by(followed_id: user.id).destroy
  end

  # フォローしているか判断する
  def following?(user)
    following.include?(user)
  end

  # その投稿がすでにお気に入りに登録してあるか判断する
  def already_likes?(micropost)
    !!get_likes(micropost)
  end

  def get_likes(micropost)
    Like.find_by(user_id: id, micropost_id: micropost.id)
  end

  # 通知用レコード作成
  def notify_to(user, instance)
    # 自分の投稿に関してのアクション(コメント、お気に入り)については
    # 通知の対象外とする
    return if self == user

    m_name = instance.class.to_s
    send_notifications.create(receiver_id: user.id,
                              m_name:      m_name,
                              r_id:        instance.id)
  end

  # 通知用レコードの削除
  def destroy_notification(user, instance)
    return if self == user

    m_name = instance.class.to_s
    send_notifications.find_by(m_name:      m_name,
                               r_id:        instance.id).destroy
  end

  # そのユーザに対する新規通知有無の判断
  def new_notifications?
    new_notifications_count.positive?
  end

  # そのユーザに対する新規通知件数を返す
  def new_notifications_count
    receive_notifications.where(confirmed: false).count
  end

  # private
  #=======================

  private

  # 属性を小文字にする
  def attr_to_downcase
    user_name.downcase!
    return if email.nil?

    email.downcase!
  end

  # 性別を数値型に変換する
  def gender_to_integer
    return if gender.blank? || gender.nil?

    self.gender = gender.to_i
  end
end
