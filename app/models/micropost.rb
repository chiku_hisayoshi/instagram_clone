class Micropost < ApplicationRecord
  # 関連性
  #=======================
  # ユーザ
  belongs_to :user
  # コメント
  has_many :comments, dependent: :destroy
  # お気に入り
  has_many :likes,    dependent: :destroy

  # バリデーション
  #=======================
  default_scope -> { order(created_at: :desc) }
  validates :content,   presence: true,
                        length: { maximum: 150 }
  validates :picture,   presence: true
  validates :user_id,   presence: true
  validate  :picture_size

  # アップローダ
  #=======================
  mount_uploader :picture, PictureUploader

  # 特異メソッド
  #=======================

  # インスタンスメソッド
  #=======================

  # private
  #=======================
  private

  # 画像のサイズに対するバリデーションを定義する
  def picture_size
    errors.add(:picture, '画像のサイズが5MBを超過しています') if picture.size > 5.megabytes
  end
end
