class Notification < ApplicationRecord
  # 関連
  #=======================
  belongs_to :sender,    class_name: 'User'
  belongs_to :receiver,  class_name: 'User'

  # バリデーション
  #=======================
  default_scope -> { order(created_at: :desc) }
  validates :sender_id,         presence: true
  validates :receiver_id,       presence: true
  validates :m_name,            presence: true
  validates :r_id,              presence: true

  # 特異メソッド
  #=======================
  class << self
    # confirmedをtrue(確認済)にする
    def update_confirmed_to_true(receiver)
      where(receiver_id: receiver.id).find_each do |notification|
        notification.update(confirmed: true)
      end
    end
  end

  # インスタンスメソッド
  #=======================

  # コメント,お気に入りから投稿を取得する
  def get_micropost
    model_name = m_name
    model_name.constantize.find_by(id: r_id).micropost
  end
end
