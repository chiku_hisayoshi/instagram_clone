class Comment < ApplicationRecord
  # 関連性
  #=======================
  belongs_to :micropost
  belongs_to :user

  # バリデーション
  #=======================
  default_scope -> { order(created_at: :desc) }
  validates :micropost_id,    presence: true
  validates :user_id,         presence: true
  validates :context,         presence: true,
                              length: { maximum: 150 }

  # インスタンスメソッド
  #=======================

  # 作成日付を日本形式で返す
  def created_at_jp_format
    created_at.strftime('%Y年%-m月%-d日')
  end
end
